﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace DynEntity.Extras
{
    public static class Extensions
    {
        public static DynamicEntity ToModel(this IEnumerable<DynamicProperty> properties)
        {
            if (properties == null || !properties.Any()) return null;

            Dictionary<string, object> pairs = new Dictionary<string, object>();
            string modelName = properties.FirstOrDefault().ModelName;
            Type type = Type.GetType(modelName);
            foreach (var property in properties)
            {
                object value = property.GetValue();
                pairs.Add(property.Name, value);
            }
            if (!pairs.Any()) return null;

            string serialized = JsonConvert.SerializeObject(pairs);
            DynamicEntity result = JsonConvert.DeserializeObject(serialized, type) as DynamicEntity;

            return result;
        }

        public static object GetValue(this DynamicProperty property)
        {
            object value = property.Name.ToLower() == "id" ? (object)property.Id : property.Value;
            if (property.RelationType == RelationType.SingleRelation)
            {
                value = property.RelationTarget.Select(r => r.Target).ToModel();
            }
            else if (property.RelationType == RelationType.MultiRelation)
            {
                List<DynamicEntity> models = new List<DynamicEntity>();

                //var groups = property.Relation.Select(r => r.Target).GroupBy(g => g.ModelId);
                var groups = property.RelationTarget.Select(r => r.Target).GroupBy(g => g.ModelId);
                foreach (var group in groups)
                {
                    IEnumerable<DynamicProperty> modelProperties = group.Select(g => g);
                    DynamicEntity model = ToModel(modelProperties);
                    models.Add(model);
                }
                value = models.Any() ? models : null;
            }

            return value;
        }

        public static List<DynamicProperty> ToProperties(this DynamicEntity model, DbSet<DynamicProperty> db)
        {
            string modelName = model.GetType().AssemblyQualifiedName;
            List<DynamicProperty> result = new List<DynamicProperty>();
            PropertyInfo[] properties = model.GetType().GetProperties();
            Guid modelId = db.GetModelId(model.Id);
            foreach (var p in properties)
            {
                object value = p.GetValue(model);
                RelationType relationType = GetRelationType(p.PropertyType);
                DynamicProperty property = new DynamicProperty();
                property.ModelId = modelId;
                property.ModelName = modelName;
                property.Name = p.Name;
                property.Type = p.PropertyType.FullName;
                property.Value = p.PropertyType.IsPrimitive || p.PropertyType == typeof(string) ?
                    p.GetValue(model)?.ToString() :
                    null;
                property.RelationType = relationType;
                result.Add(property);

                //Recursive
                if (value != null)
                {
                    if (relationType == RelationType.SingleRelation)
                    {
                        List<DynamicProperty> nested = (value as DynamicEntity).ToProperties(db);
                        //List<DynamicRelation> relations = nested.Select(n => new DynamicRelation()
                        //{
                        //    Base = property,
                        //    Target = n
                        //}).ToList();
                        //foreach (var item in nested)
                        //{
                        //    DynamicRelation relation = new DynamicRelation()
                        //    {
                        //        Base = property,
                        //        Target = item
                        //    };
                        //    property.Relation.Add(relation);
                        //}
                    }
                    else if (relationType == RelationType.MultiRelation)
                    {
                        IEnumerable<DynamicEntity> asEnumable = value as IEnumerable<DynamicEntity>;
                        foreach (var entity in asEnumable)
                        {
                            List<DynamicProperty> nested = entity.ToProperties(db);
                            //foreach (var item in nested)
                            //{
                            //    DynamicRelation relation = new DynamicRelation()
                            //    {
                            //        Base = property,
                            //        Target = item
                            //    };
                            //    property.Relation.Add(relation);
                            //}
                        }
                    }
                }
            }
            return result;

        }

        public static Guid GetModelId(this DbSet<DynamicProperty> db, int id)
        {
            DynamicProperty idProperty = db.FirstOrDefault(p => p.Id == id);
            Guid modelId = idProperty != null ? idProperty.ModelId : Guid.NewGuid();
            return modelId;
        }
        public static RelationType GetRelationType(this Type type)
        {
            if (typeof(DynamicEntity).IsAssignableFrom(type))
            {
                return RelationType.SingleRelation;
            }
            else if (typeof(IEnumerable<DynamicEntity>).IsAssignableFrom(type))
            {
                return RelationType.MultiRelation;
            }
            else
            {
                return RelationType.None;
            }
        }

    }
}
