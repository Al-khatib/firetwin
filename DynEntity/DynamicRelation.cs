﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DynEntity
{
    public class DynamicRelation
    {
        public int Id { get; set; }
        public int BaseId { get; set; }
        public int TargetId { get; set; }

        public virtual DynamicProperty Base { get; set; }
        public virtual DynamicProperty Target { get; set; }

    }

}

