﻿using DynEntity.Extras;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace DynEntity
{
    public class DataRepository<T> where T : DynamicEntity
    {
        private readonly DbContext _context;
        private readonly DbSet<DynamicProperty> _db;
        private readonly string Modelname = typeof(T).AssemblyQualifiedName;

        public DataRepository(DbContext context)
        {
            _context = context;
            _db = context.Set<DynamicProperty>();
        }


        /// <summary>
        /// List all entities
        /// </summary>
        /// <returns></returns>
        public List<T> All()
        {
            var filtered = _db.Where(p => p.ModelName == Modelname).ToList();
            var grouped = filtered.GroupBy(p => p.ModelId).Select(g => g.ToList());
            var result = grouped.Select(g => g.ToModel() as T).ToList();
            return result;
        }

        public T Find(int id)
        {
            T model = FindUnstrict(id) as T;
            return model;
        }

        public int Add(T model)
        {
            int id = AddUnstrict(model);
            return id;
        }

        /// <summary>
        /// Will return false if it fails for any reason
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Remove(int id)
        {
            DynamicProperty idProperty = _db.Find(id);
            if (idProperty == null) return false;
            Guid modelId = idProperty.ModelId;
            IEnumerable<DynamicProperty> modelProperties = _db.Where(p => p.ModelId == modelId && p.ModelName == Modelname);
            if (modelProperties != null)
            {
                _db.RemoveRange(modelProperties);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Returns the id of the updated entity
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int Update(T model)
        {
            int id = UpdateUnstrict(model);
            return id;
        }
        public DynamicEntity FindUnstrict(int id)
        {
            DynamicProperty idProperty = _db.Find(id);
            if (idProperty == null) return null;
            Guid modelId = idProperty.ModelId;
            List<DynamicProperty> modelProperties = _db.Where(p => p.ModelId == modelId).ToList();
            DynamicEntity model = modelProperties.ToModel();
            return model;
        }

        private int AddUnstrict(DynamicEntity model)
        {
            List<DynamicProperty> properties = model.ToProperties(_db);
            _db.AddRange(properties);
            _context.SaveChanges();
            int id = properties.FirstOrDefault(p => p.Name.ToLower() == "id").Id;
            return id;
        }

        private int UpdateUnstrict(DynamicEntity model)
        {
            DynamicProperty idProperty = _db.Find(model.Id);
            if (idProperty == null)
            {
                int id = AddUnstrict(model);
                return id;
            }
            else
            {
                Guid modelId = idProperty.ModelId;
                List<DynamicProperty> properties = _db.Include(x => x.Relation).Where(p => p.ModelId == modelId).ToList();
                PropertyInfo[] infos = model.GetType().GetProperties();
                foreach (var info in infos)
                {
                    DynamicProperty property = properties.FirstOrDefault(p => p.Name == info.Name);
                    RelationType relationType = info.PropertyType.GetRelationType();
                    object value = info.GetValue(model);
                    if (relationType == RelationType.SingleRelation)
                    {
                        DynamicEntity relation = value as DynamicEntity;
                        int id = UpdateUnstrict(relation);
                        DynamicProperty relationIdProperty = _db.Find(id);
                        List<DynamicProperty> relationProperties = _db.Where(p => p.ModelId == relationIdProperty.ModelId).ToList();
                        property.SetRelations(relationProperties);
                        _context.UpdateRange(relationProperties);
                    }
                    else if (relationType == RelationType.MultiRelation)
                    {
                        IEnumerable<DynamicEntity> relationEnttities = value as IEnumerable<DynamicEntity>;
                        foreach (var entity in relationEnttities)
                        {
                            int id = UpdateUnstrict(entity);
                            DynamicProperty relationIdProperty = _db.Find(id);
                            List<DynamicProperty> relationProperties = _db.Where(p => p.ModelId == relationIdProperty.ModelId).ToList();
                            _context.RemoveRange(property.Relation);
                            property.SetRelations(relationProperties);

                            IEnumerable<DynamicRelation> relations = relationProperties.Select(p => new DynamicRelation()
                            {
                                BaseId = property.Id,
                                Base = property,
                                TargetId = p.Id,
                                Target = p
                            });
                            _context.AddRange(relations);
                        }
                    }
                    else
                    {
                        property.Value = info.PropertyType.IsPrimitive || info.PropertyType == typeof(string) ?
                            value?.ToString() :
                            null;
                    }
                    //_db.Update(property);
                    try
                    {
                        _context.Update(property);
                        _context.SaveChanges();
                    }
                    catch (Exception ex)
                    {

                        throw;
                    }
                }
                return model.Id;
            }
        }
    }
}
