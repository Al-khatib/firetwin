﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DynEntity
{
    public abstract class DynamicEntity : IDataReposible
    {
        public int Id { get; set; }
    }
}
