﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DynEntity
{

    public enum RelationType
    {
        None,
        SingleRelation,
        MultiRelation
    }

    public class DynamicProperty
    {
        public DynamicProperty()
        {
            Relation = new List<DynamicRelation>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }

        public string ModelName { get; set; }
        public RelationType RelationType { get; set; }

        public virtual Guid ModelId { get; set; }

        public virtual ICollection<DynamicRelation> Relation { get; set; }
        public virtual ICollection<DynamicRelation> RelationTarget { get; set; }

        public void SetRelations(IEnumerable<DynamicProperty> properties)
        {
            IEnumerable<DynamicRelation> relations = properties.Select(p => new DynamicRelation()
            {
                BaseId = this.Id,
                Base = this,
                TargetId = p.Id,
                Target = p
            });
            //this.Relation = relations.ToList();
        }
    }
}
