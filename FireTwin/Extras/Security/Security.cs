﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FireTwin.Extras.Security
{
    public static class Security
    {
        //Add more roles here if needed
        public enum Roles
        {
            Admin,
            Editor,
            User
        }
        public enum Policies
        {
            AdminOnly
        }

        /// <summary>
        /// Return string value of the enum
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public static string ToText<T>(this T role) where T : struct, IConvertible
        {
            return typeof(T).IsEnum ? Enum.GetName(typeof(T), role) : role.ToString();
        }



        public static async Task FeedDefaultRoles(IServiceProvider serviceProvider)
        {
            var RoleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            var roles = Enum.GetNames(typeof(Roles));
            foreach (string role in roles)
            {
                Policies.AdminOnly.ToText();
                bool exists = await RoleManager.RoleExistsAsync(role);
                if (!exists)
                {
                    await RoleManager.CreateAsync(new IdentityRole(role));
                }
            }
        }

        public static async Task FeedDefaultUsers(IServiceProvider serviceProvider)
        {
            var UserManager = serviceProvider.GetRequiredService<UserManager<IdentityUser>>();
            //Add default users here
            IdentityUser user1 = new IdentityUser()
            {
                UserName = "molham@molham.com",
                Email = "molham@molham.com",
                SecurityStamp = Guid.NewGuid().ToString("D")
            };
            await CreateUser(UserManager, user1, Roles.Admin, "TopSecrit1!");

            IdentityUser user2 = new IdentityUser()
            {
                UserName = "firas@firas.com",
                Email = "firas@firas.com",
                SecurityStamp = Guid.NewGuid().ToString("D")
            };
            await CreateUser(UserManager, user2, Roles.Admin, "TopSecrit1!");
        }

        public static async Task CreateUser(UserManager<IdentityUser> manager, IdentityUser user, Roles role, string password)
        {
            bool exists = (await manager.FindByEmailAsync(user.Email)) != null;
            if (!exists)
            {
                var x = role.ToText();
                var res = manager.CreateAsync(user, password).Result;
                if (!res.Succeeded)
                {
                    throw new Exception(
                        res.Errors.
                        Select(e => e.Description)
                        .Aggregate((i, j) => i + "," + j)
                        );
                }
                else
                {
                    manager.AddToRoleAsync(user, role.ToText()).Wait();
                }

            }


        }

    }
}
