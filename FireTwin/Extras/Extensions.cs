﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;

namespace FireTwin.Extras
{
    public static class Extensions
    {
        //public static string GetBase64(this Image image)
        //{
        //    string extension = Path.GetExtension(image.Path);
        //    byte[] bytes = File.ReadAllBytes(Bundler.Uploaded + image.Path);

        //    string base64 = string.Format("data:image/{0};base64,{1}", extension, Convert.ToBase64String(bytes));
        //    return base64;

        //}

        public static JsonResult ToJsonResult(this object obj, bool useCamelCase = true)
        {
            var settings = new Newtonsoft.Json.JsonSerializerSettings();
            settings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            if (useCamelCase)
                settings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            return new JsonResult(obj, settings);
        }


    }

    public static class SessionExtensions
    {
        public static void SetObjectAsJson(this ISession session, string key, object value)
        {
            session.SetString(key, JsonConvert.SerializeObject(value));
        }

        public static T GetObjectFromJson<T>(this ISession session, string key)
        {
            var value = session.GetString(key);

            return value == null ? default(T) : JsonConvert.DeserializeObject<T>(value);
        }
    }

}
