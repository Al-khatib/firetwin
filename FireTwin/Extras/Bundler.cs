﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;



namespace FireTwin.Extras
{
    public static class Bundler
    {

        public static IHostingEnvironment HostingEnviroment;
        public static string MediaFolder;

        private const string rootFolder = "js";
        private const string layoutFolder = "layout";

        public static HtmlString RenderTsScripts(this IHtmlHelper helper, string layoutName)
        {
            string extension = HostingEnviroment.IsDevelopment() ? "js" : "map.js";

            string controller = helper.ViewContext.RouteData.Values["controller"]?.ToString();
            string view = helper.ViewContext.RouteData.Values["action"]?.ToString();

            string scriptPath = controller != null && view != null ?
                string.Format("/{0}/{1}/{2}.{3}", rootFolder, controller, view, extension) :
                string.Empty;
            string layoutScriptPath = string.Format("/{0}/{1}/{2}.{3}", rootFolder, layoutFolder, layoutName, extension);

            bool layoutScriptExists = System.IO.File.Exists(HostingEnviroment.WebRootPath + layoutScriptPath);

            bool scriptExists = !string.IsNullOrWhiteSpace(scriptPath) ?
                System.IO.File.Exists(HostingEnviroment.WebRootPath + scriptPath) :
                false;

            string tag = "<script src=\"{0}\"></script>";
            string layoutTag = layoutScriptExists ? string.Format(tag, layoutScriptPath) : null;
            string scriptTag = scriptExists ? string.Format(tag, scriptPath) : null;

            return new HtmlString(layoutTag + scriptTag);
        }



    }
}