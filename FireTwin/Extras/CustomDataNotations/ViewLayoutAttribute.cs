﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FireTwin.Extras.CustomDataNotations
{
    public class ViewLayoutAttribute : ResultFilterAttribute
    {
        public bool AdminLayout;
        public ViewLayoutAttribute()
        {
        }

        public override void OnResultExecuting(ResultExecutingContext context)
        {
            base.OnResultExecuting(context);
            var result = context.Result as ViewResult;
            if (result != null && AdminLayout)
            {
                result.ViewData["UseAdminLayout"] = true;
            }
        }
    }
}
