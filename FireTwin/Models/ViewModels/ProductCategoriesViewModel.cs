﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FireTwin.Models.ViewModels
{
    public class ProductCategoriesViewModel
    {
        private IEnumerable<Category> _categories;


        public Product Product { get; set; }
        public List<SelectListItem> CategoriesSelect { get; set; }

        public int SelectedCategoryId { get; set; }





        public IEnumerable<Category> Categories
        {
            get { return _categories; }
            set
            {
                CategoriesSelect = value.Select(c => new SelectListItem { Value = c.Id.ToString(), Text = c.Name }).ToList();
                _categories = value;
            }
        }

    }
}