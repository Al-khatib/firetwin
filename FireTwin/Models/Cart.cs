﻿using DynEntity;
using System.Collections.Generic;

namespace FireTwin.Models
{
    public class Cart : DynamicEntity
    {
        public string UserId { get; set; }

        public List<Product> Products { get; set; }
    }
}
