﻿using DynEntity;

namespace FireTwin.Models
{
    public class Image : DynamicEntity
    {
        public string Name { get; set; }
        public string Path { get; set; }
        public bool Private { get; set; }

    }
}
