﻿using DynEntity;
using System.Collections.Generic;

namespace FireTwin.Models
{
    public class Folder : DynamicEntity
    {
        public string Name { get; set; }
        public ICollection<Image> Images { get; set; }


    }
}
