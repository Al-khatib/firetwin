﻿using DynEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FireTwin.Models
{
    public class Category : DynamicEntity
    {

        [Required]
        public string Name { get; set; }
        public virtual ICollection<Product> Products { get; set; }
        public virtual Category SubCategory { get; set; }


        //Only for testing now
        public Product Product { get; set; }


    }
}
