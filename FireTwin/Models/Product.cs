﻿using DynEntity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FireTwin.Models
{
    public class Product : DynamicEntity
    {
        public Product()
        {
            Images = new List<Image>();
        }
        [Required]
        [MaxLength(45)]
        public string Name { get; set; }
        public string Description { get; set; }
        public string LongDescription { get; set; }

        [Required]
        public int Price { get; set; }

        public Category Category { get; set; }
        public IEnumerable<Image> Images { get; set; }
    }

}

