﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using FireTwin.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using FireTwin.Extras;
using FireTwin.Extras.Security;
using System;
using DynEntity;

namespace FireTwin
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseLazyLoadingProxies()
                .UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            services.AddIdentity<IdentityUser, IdentityRole>()
                .AddDefaultUI()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();


            services.AddDistributedMemoryCache();
            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromDays(90);
            });

            //services.AddDistributedSqlServerCache(o =>
            //{
            //    o.ConnectionString = Configuration.GetConnectionString("DefaultConnection");
            //    o.SchemaName = "dbo";
            //    o.TableName = "Sessions";
            //});

            services.AddMvc();

            services.AddAuthorization(options =>
            {
                options.AddPolicy(Security.Policies.AdminOnly.ToText(), policy =>
                {
                    //Add more roles if required

                    policy.RequireRole(Security.Roles.Admin.ToText());
                });
            });
            InitializeDatabase(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {


            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseAuthentication();

            // IMPORTANT: This session call MUST go before UseMvc()
            app.UseSession();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            Bundler.HostingEnviroment = env;
            Bundler.MediaFolder = "Media";

        }

        private void InitializeDatabase(IServiceCollection services)
        {

            ServiceProvider provider = services.BuildServiceProvider();
            ApplicationDbContext context = provider.GetRequiredService<ApplicationDbContext>();
            Security.FeedDefaultRoles(provider).Wait();
            Security.FeedDefaultUsers(provider).Wait();
            //services.AddScoped<DataFactory>();
            services.AddScoped(typeof(DbContext), typeof(ApplicationDbContext));
            services.AddScoped(typeof(DataRepository<>), typeof(DataRepository<>));

        }

    }
}
