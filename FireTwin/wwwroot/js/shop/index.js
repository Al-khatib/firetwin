/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/shop/Index.ts");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/css-loader/dist/cjs.js!./src/modules/css/ProductCard.css":
/*!*******************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/modules/css/ProductCard.css ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js")(false);
// Module
exports.push([module.i, "\r\n\r\n.product-container {\r\n    padding: .8em;\r\n    background-color: #fff;\r\n    margin: 10px;\r\n    flex: 1 0 calc(20% - 10px);\r\n    cursor: pointer;\r\n}\r\n\r\n    .product-container:hover {\r\n        box-shadow: 5px 5px 5px 5px #eee;\r\n        border: 1px solid #eee;\r\n        transform: scale(1.1);\r\n        z-index: 100;\r\n        transition: box-shadow 400ms ease, transform 400ms ease;\r\n    }\r\n\r\n\r\n.product-image-container {\r\n    width: 100%;\r\n    padding-top: 100%;\r\n    position: relative;\r\n}\r\n\r\n\r\n.product-image {\r\n    width: 100%;\r\n    position: absolute;\r\n    top: 0;\r\n    left: 0;\r\n}\r\n\r\n.product-label {\r\n    width: 100%;\r\n    margin-top: .8em;\r\n    text-align: center;\r\n    font-weight: 100;\r\n}\r\n\r\n@media only screen and (max-width: 768px) {\r\n    /* For mobile phones: */\r\n    .product-container {\r\n        flex: 1 0 calc(100% - 10px);\r\n    }\r\n}\r\n", ""]);


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/modules/css/folder-card.css":
/*!*******************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/modules/css/folder-card.css ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js")(false);
// Module
exports.push([module.i, ".folder-card {\r\n    padding: 7px;\r\n    cursor: pointer;\r\n    display: flex;\r\n    flex-direction: column;\r\n}\r\n\r\n    .folder-card > img {\r\n        width: 100%;\r\n        margin: auto;\r\n    }\r\n\r\n    .folder-card > .folder-card-name {\r\n        text-align: center;\r\n        overflow: hidden;\r\n        text-overflow: ellipsis;\r\n        white-space: nowrap;\r\n        margin-top: auto;\r\n    }\r\n\r\n.folder-card-selected {\r\n    border-radius: 7px;\r\n    background-color: cornflowerblue;\r\n}\r\n\r\n.icon-folder {\r\n    padding: 2%;\r\n    width: 8%;\r\n}\r\n\r\n    .icon-folder > a > img {\r\n        height: auto;\r\n        width: 100%;\r\n    }\r\n\r\n.folder-name {\r\n    text-align: center;\r\n    margin: 0;\r\n}\r\n", ""]);


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/modules/css/modal.css":
/*!*************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/modules/css/modal.css ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js")(false);
// Module
exports.push([module.i, ".modal-window-background {\r\n    height: 100vh;\r\n    width: 100vw;\r\n    position: absolute;\r\n    top: 0;\r\n    left: 0;\r\n    background-color: rgba(100,100,100,0.3);\r\n}\r\n\r\n.modal-window-container {\r\n    display: flex;\r\n    flex-direction: column;\r\n    position: absolute;\r\n    left: 50%;\r\n    top: 50%;\r\n    -ms-transform: translate(-50%,-50%); /* IE 9 */\r\n    -webkit-transform: translate(-50%,-50%); /* Safari */\r\n    transform: translate(-50%,-50%);\r\n    background-color: white;\r\n    border-radius: 7px;\r\n}\r\n\r\n\r\n.modal-window-header {\r\n    background-color: cornflowerblue;\r\n    padding: 5px;\r\n    border-top-left-radius: 7px;\r\n    border-top-right-radius: 7px;\r\n}\r\n\r\n.modal-window-close {\r\n    float: right;\r\n}\r\n\r\n.modal-window-body {\r\n    padding: 20px;\r\n    flex-grow: 1;\r\n    position: relative;\r\n}\r\n\r\n    .modal-window-body input {\r\n        width: 20em;\r\n    }\r\n\r\n.modal-window-ok {\r\n    float: right;\r\n}\r\n\r\n.modal-window-footer {\r\n    border-top: 1px solid #bbb;\r\n    padding: 10px 20px;\r\n    margin-top: auto;\r\n}\r\n", ""]);


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/modules/css/spinner.css":
/*!***************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/modules/css/spinner.css ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js")(false);
// Module
exports.push([module.i, ".spinner-container {\r\n    display: flex;\r\n    justify-content: center;\r\n    align-items: center;\r\n    background-color: rgba(100,100,100,0.3);\r\n}\r\n\r\n\r\n.spinner-ring {\r\n    display: inline-block;\r\n    position: relative;\r\n    width: 64px;\r\n    height: 64px;\r\n}\r\n\r\n    .spinner-ring div {\r\n        box-sizing: border-box;\r\n        display: block;\r\n        position: absolute;\r\n        width: 80%;\r\n        height: 80%;\r\n        margin: 6px;\r\n        border: 6px solid #fff;\r\n        border-radius: 50%;\r\n        animation: spinner-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;\r\n        border-color: #07d transparent transparent transparent;\r\n    }\r\n\r\n        .spinner-ring div:nth-child(1) {\r\n            animation-delay: -0.45s;\r\n        }\r\n\r\n        .spinner-ring div:nth-child(2) {\r\n            animation-delay: -0.3s;\r\n        }\r\n\r\n        .spinner-ring div:nth-child(3) {\r\n            animation-delay: -0.15s;\r\n        }\r\n\r\n@keyframes spinner-ring {\r\n    0% {\r\n        transform: rotate(0deg);\r\n    }\r\n\r\n    100% {\r\n        transform: rotate(360deg);\r\n    }\r\n}\r\n", ""]);


/***/ }),

/***/ "./node_modules/css-loader/dist/runtime/api.js":
/*!*****************************************************!*\
  !*** ./node_modules/css-loader/dist/runtime/api.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
// eslint-disable-next-line func-names
module.exports = function (useSourceMap) {
  var list = []; // return the list of modules as css string

  list.toString = function toString() {
    return this.map(function (item) {
      var content = cssWithMappingToString(item, useSourceMap);

      if (item[2]) {
        return "@media ".concat(item[2], "{").concat(content, "}");
      }

      return content;
    }).join('');
  }; // import a list of modules into the list
  // eslint-disable-next-line func-names


  list.i = function (modules, mediaQuery) {
    if (typeof modules === 'string') {
      // eslint-disable-next-line no-param-reassign
      modules = [[null, modules, '']];
    }

    var alreadyImportedModules = {};

    for (var i = 0; i < this.length; i++) {
      // eslint-disable-next-line prefer-destructuring
      var id = this[i][0];

      if (id != null) {
        alreadyImportedModules[id] = true;
      }
    }

    for (var _i = 0; _i < modules.length; _i++) {
      var item = modules[_i]; // skip already imported module
      // this implementation is not 100% perfect for weird media query combinations
      // when a module is imported multiple times with different media queries.
      // I hope this will never occur (Hey this way we have smaller bundles)

      if (item[0] == null || !alreadyImportedModules[item[0]]) {
        if (mediaQuery && !item[2]) {
          item[2] = mediaQuery;
        } else if (mediaQuery) {
          item[2] = "(".concat(item[2], ") and (").concat(mediaQuery, ")");
        }

        list.push(item);
      }
    }
  };

  return list;
};

function cssWithMappingToString(item, useSourceMap) {
  var content = item[1] || ''; // eslint-disable-next-line prefer-destructuring

  var cssMapping = item[3];

  if (!cssMapping) {
    return content;
  }

  if (useSourceMap && typeof btoa === 'function') {
    var sourceMapping = toComment(cssMapping);
    var sourceURLs = cssMapping.sources.map(function (source) {
      return "/*# sourceURL=".concat(cssMapping.sourceRoot).concat(source, " */");
    });
    return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
  }

  return [content].join('\n');
} // Adapted from convert-source-map (MIT)


function toComment(sourceMap) {
  // eslint-disable-next-line no-undef
  var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
  var data = "sourceMappingURL=data:application/json;charset=utf-8;base64,".concat(base64);
  return "/*# ".concat(data, " */");
}

/***/ }),

/***/ "./node_modules/style-loader/lib/addStyles.js":
/*!****************************************************!*\
  !*** ./node_modules/style-loader/lib/addStyles.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/

var stylesInDom = {};

var	memoize = function (fn) {
	var memo;

	return function () {
		if (typeof memo === "undefined") memo = fn.apply(this, arguments);
		return memo;
	};
};

var isOldIE = memoize(function () {
	// Test for IE <= 9 as proposed by Browserhacks
	// @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805
	// Tests for existence of standard globals is to allow style-loader
	// to operate correctly into non-standard environments
	// @see https://github.com/webpack-contrib/style-loader/issues/177
	return window && document && document.all && !window.atob;
});

var getTarget = function (target, parent) {
  if (parent){
    return parent.querySelector(target);
  }
  return document.querySelector(target);
};

var getElement = (function (fn) {
	var memo = {};

	return function(target, parent) {
                // If passing function in options, then use it for resolve "head" element.
                // Useful for Shadow Root style i.e
                // {
                //   insertInto: function () { return document.querySelector("#foo").shadowRoot }
                // }
                if (typeof target === 'function') {
                        return target();
                }
                if (typeof memo[target] === "undefined") {
			var styleTarget = getTarget.call(this, target, parent);
			// Special case to return head of iframe instead of iframe itself
			if (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {
				try {
					// This will throw an exception if access to iframe is blocked
					// due to cross-origin restrictions
					styleTarget = styleTarget.contentDocument.head;
				} catch(e) {
					styleTarget = null;
				}
			}
			memo[target] = styleTarget;
		}
		return memo[target]
	};
})();

var singleton = null;
var	singletonCounter = 0;
var	stylesInsertedAtTop = [];

var	fixUrls = __webpack_require__(/*! ./urls */ "./node_modules/style-loader/lib/urls.js");

module.exports = function(list, options) {
	if (typeof DEBUG !== "undefined" && DEBUG) {
		if (typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
	}

	options = options || {};

	options.attrs = typeof options.attrs === "object" ? options.attrs : {};

	// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
	// tags it will allow on a page
	if (!options.singleton && typeof options.singleton !== "boolean") options.singleton = isOldIE();

	// By default, add <style> tags to the <head> element
        if (!options.insertInto) options.insertInto = "head";

	// By default, add <style> tags to the bottom of the target
	if (!options.insertAt) options.insertAt = "bottom";

	var styles = listToStyles(list, options);

	addStylesToDom(styles, options);

	return function update (newList) {
		var mayRemove = [];

		for (var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];

			domStyle.refs--;
			mayRemove.push(domStyle);
		}

		if(newList) {
			var newStyles = listToStyles(newList, options);
			addStylesToDom(newStyles, options);
		}

		for (var i = 0; i < mayRemove.length; i++) {
			var domStyle = mayRemove[i];

			if(domStyle.refs === 0) {
				for (var j = 0; j < domStyle.parts.length; j++) domStyle.parts[j]();

				delete stylesInDom[domStyle.id];
			}
		}
	};
};

function addStylesToDom (styles, options) {
	for (var i = 0; i < styles.length; i++) {
		var item = styles[i];
		var domStyle = stylesInDom[item.id];

		if(domStyle) {
			domStyle.refs++;

			for(var j = 0; j < domStyle.parts.length; j++) {
				domStyle.parts[j](item.parts[j]);
			}

			for(; j < item.parts.length; j++) {
				domStyle.parts.push(addStyle(item.parts[j], options));
			}
		} else {
			var parts = [];

			for(var j = 0; j < item.parts.length; j++) {
				parts.push(addStyle(item.parts[j], options));
			}

			stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
		}
	}
}

function listToStyles (list, options) {
	var styles = [];
	var newStyles = {};

	for (var i = 0; i < list.length; i++) {
		var item = list[i];
		var id = options.base ? item[0] + options.base : item[0];
		var css = item[1];
		var media = item[2];
		var sourceMap = item[3];
		var part = {css: css, media: media, sourceMap: sourceMap};

		if(!newStyles[id]) styles.push(newStyles[id] = {id: id, parts: [part]});
		else newStyles[id].parts.push(part);
	}

	return styles;
}

function insertStyleElement (options, style) {
	var target = getElement(options.insertInto)

	if (!target) {
		throw new Error("Couldn't find a style target. This probably means that the value for the 'insertInto' parameter is invalid.");
	}

	var lastStyleElementInsertedAtTop = stylesInsertedAtTop[stylesInsertedAtTop.length - 1];

	if (options.insertAt === "top") {
		if (!lastStyleElementInsertedAtTop) {
			target.insertBefore(style, target.firstChild);
		} else if (lastStyleElementInsertedAtTop.nextSibling) {
			target.insertBefore(style, lastStyleElementInsertedAtTop.nextSibling);
		} else {
			target.appendChild(style);
		}
		stylesInsertedAtTop.push(style);
	} else if (options.insertAt === "bottom") {
		target.appendChild(style);
	} else if (typeof options.insertAt === "object" && options.insertAt.before) {
		var nextSibling = getElement(options.insertAt.before, target);
		target.insertBefore(style, nextSibling);
	} else {
		throw new Error("[Style Loader]\n\n Invalid value for parameter 'insertAt' ('options.insertAt') found.\n Must be 'top', 'bottom', or Object.\n (https://github.com/webpack-contrib/style-loader#insertat)\n");
	}
}

function removeStyleElement (style) {
	if (style.parentNode === null) return false;
	style.parentNode.removeChild(style);

	var idx = stylesInsertedAtTop.indexOf(style);
	if(idx >= 0) {
		stylesInsertedAtTop.splice(idx, 1);
	}
}

function createStyleElement (options) {
	var style = document.createElement("style");

	if(options.attrs.type === undefined) {
		options.attrs.type = "text/css";
	}

	if(options.attrs.nonce === undefined) {
		var nonce = getNonce();
		if (nonce) {
			options.attrs.nonce = nonce;
		}
	}

	addAttrs(style, options.attrs);
	insertStyleElement(options, style);

	return style;
}

function createLinkElement (options) {
	var link = document.createElement("link");

	if(options.attrs.type === undefined) {
		options.attrs.type = "text/css";
	}
	options.attrs.rel = "stylesheet";

	addAttrs(link, options.attrs);
	insertStyleElement(options, link);

	return link;
}

function addAttrs (el, attrs) {
	Object.keys(attrs).forEach(function (key) {
		el.setAttribute(key, attrs[key]);
	});
}

function getNonce() {
	if (false) {}

	return __webpack_require__.nc;
}

function addStyle (obj, options) {
	var style, update, remove, result;

	// If a transform function was defined, run it on the css
	if (options.transform && obj.css) {
	    result = typeof options.transform === 'function'
		 ? options.transform(obj.css) 
		 : options.transform.default(obj.css);

	    if (result) {
	    	// If transform returns a value, use that instead of the original css.
	    	// This allows running runtime transformations on the css.
	    	obj.css = result;
	    } else {
	    	// If the transform function returns a falsy value, don't add this css.
	    	// This allows conditional loading of css
	    	return function() {
	    		// noop
	    	};
	    }
	}

	if (options.singleton) {
		var styleIndex = singletonCounter++;

		style = singleton || (singleton = createStyleElement(options));

		update = applyToSingletonTag.bind(null, style, styleIndex, false);
		remove = applyToSingletonTag.bind(null, style, styleIndex, true);

	} else if (
		obj.sourceMap &&
		typeof URL === "function" &&
		typeof URL.createObjectURL === "function" &&
		typeof URL.revokeObjectURL === "function" &&
		typeof Blob === "function" &&
		typeof btoa === "function"
	) {
		style = createLinkElement(options);
		update = updateLink.bind(null, style, options);
		remove = function () {
			removeStyleElement(style);

			if(style.href) URL.revokeObjectURL(style.href);
		};
	} else {
		style = createStyleElement(options);
		update = applyToTag.bind(null, style);
		remove = function () {
			removeStyleElement(style);
		};
	}

	update(obj);

	return function updateStyle (newObj) {
		if (newObj) {
			if (
				newObj.css === obj.css &&
				newObj.media === obj.media &&
				newObj.sourceMap === obj.sourceMap
			) {
				return;
			}

			update(obj = newObj);
		} else {
			remove();
		}
	};
}

var replaceText = (function () {
	var textStore = [];

	return function (index, replacement) {
		textStore[index] = replacement;

		return textStore.filter(Boolean).join('\n');
	};
})();

function applyToSingletonTag (style, index, remove, obj) {
	var css = remove ? "" : obj.css;

	if (style.styleSheet) {
		style.styleSheet.cssText = replaceText(index, css);
	} else {
		var cssNode = document.createTextNode(css);
		var childNodes = style.childNodes;

		if (childNodes[index]) style.removeChild(childNodes[index]);

		if (childNodes.length) {
			style.insertBefore(cssNode, childNodes[index]);
		} else {
			style.appendChild(cssNode);
		}
	}
}

function applyToTag (style, obj) {
	var css = obj.css;
	var media = obj.media;

	if(media) {
		style.setAttribute("media", media)
	}

	if(style.styleSheet) {
		style.styleSheet.cssText = css;
	} else {
		while(style.firstChild) {
			style.removeChild(style.firstChild);
		}

		style.appendChild(document.createTextNode(css));
	}
}

function updateLink (link, options, obj) {
	var css = obj.css;
	var sourceMap = obj.sourceMap;

	/*
		If convertToAbsoluteUrls isn't defined, but sourcemaps are enabled
		and there is no publicPath defined then lets turn convertToAbsoluteUrls
		on by default.  Otherwise default to the convertToAbsoluteUrls option
		directly
	*/
	var autoFixUrls = options.convertToAbsoluteUrls === undefined && sourceMap;

	if (options.convertToAbsoluteUrls || autoFixUrls) {
		css = fixUrls(css);
	}

	if (sourceMap) {
		// http://stackoverflow.com/a/26603875
		css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
	}

	var blob = new Blob([css], { type: "text/css" });

	var oldSrc = link.href;

	link.href = URL.createObjectURL(blob);

	if(oldSrc) URL.revokeObjectURL(oldSrc);
}


/***/ }),

/***/ "./node_modules/style-loader/lib/urls.js":
/*!***********************************************!*\
  !*** ./node_modules/style-loader/lib/urls.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {


/**
 * When source maps are enabled, `style-loader` uses a link element with a data-uri to
 * embed the css on the page. This breaks all relative urls because now they are relative to a
 * bundle instead of the current page.
 *
 * One solution is to only use full urls, but that may be impossible.
 *
 * Instead, this function "fixes" the relative urls to be absolute according to the current page location.
 *
 * A rudimentary test suite is located at `test/fixUrls.js` and can be run via the `npm test` command.
 *
 */

module.exports = function (css) {
  // get current location
  var location = typeof window !== "undefined" && window.location;

  if (!location) {
    throw new Error("fixUrls requires window.location");
  }

	// blank or null?
	if (!css || typeof css !== "string") {
	  return css;
  }

  var baseUrl = location.protocol + "//" + location.host;
  var currentDir = baseUrl + location.pathname.replace(/\/[^\/]*$/, "/");

	// convert each url(...)
	/*
	This regular expression is just a way to recursively match brackets within
	a string.

	 /url\s*\(  = Match on the word "url" with any whitespace after it and then a parens
	   (  = Start a capturing group
	     (?:  = Start a non-capturing group
	         [^)(]  = Match anything that isn't a parentheses
	         |  = OR
	         \(  = Match a start parentheses
	             (?:  = Start another non-capturing groups
	                 [^)(]+  = Match anything that isn't a parentheses
	                 |  = OR
	                 \(  = Match a start parentheses
	                     [^)(]*  = Match anything that isn't a parentheses
	                 \)  = Match a end parentheses
	             )  = End Group
              *\) = Match anything and then a close parens
          )  = Close non-capturing group
          *  = Match anything
       )  = Close capturing group
	 \)  = Match a close parens

	 /gi  = Get all matches, not the first.  Be case insensitive.
	 */
	var fixedCss = css.replace(/url\s*\(((?:[^)(]|\((?:[^)(]+|\([^)(]*\))*\))*)\)/gi, function(fullMatch, origUrl) {
		// strip quotes (if they exist)
		var unquotedOrigUrl = origUrl
			.trim()
			.replace(/^"(.*)"$/, function(o, $1){ return $1; })
			.replace(/^'(.*)'$/, function(o, $1){ return $1; });

		// already a full url? no change
		if (/^(#|data:|http:\/\/|https:\/\/|file:\/\/\/|\s*$)/i.test(unquotedOrigUrl)) {
		  return fullMatch;
		}

		// convert the url to a full url
		var newUrl;

		if (unquotedOrigUrl.indexOf("//") === 0) {
		  	//TODO: should we add protocol?
			newUrl = unquotedOrigUrl;
		} else if (unquotedOrigUrl.indexOf("/") === 0) {
			// path should be relative to the base url
			newUrl = baseUrl + unquotedOrigUrl; // already starts with '/'
		} else {
			// path should be relative to current directory
			newUrl = currentDir + unquotedOrigUrl.replace(/^\.\//, ""); // Strip leading './'
		}

		// send back the fixed url(...)
		return "url(" + JSON.stringify(newUrl) + ")";
	});

	// send back the fixed css
	return fixedCss;
};


/***/ }),

/***/ "./src/modules/HttpRequest.ts":
/*!************************************!*\
  !*** ./src/modules/HttpRequest.ts ***!
  \************************************/
/*! exports provided: HttpRequest */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpRequest", function() { return HttpRequest; });
class HttpRequest {
    static Post(url, parameters, onSuccess, onError) {
        let xhr = new XMLHttpRequest();
        xhr.open('POST', url, true);
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {
                onSuccess(xhr.responseText);
            }
        };
        xhr.onerror = (err) => {
            if (onError) {
                onError(err);
            }
        };
        if (HttpRequest.HasNestedChildren(parameters)) {
            xhr.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
            let data = JSON.stringify(parameters);
            xhr.send(data);
        }
        else {
            let form = new FormData();
            for (let p in parameters) {
                form.append(p, parameters[p]);
            }
            xhr.send(form);
        }
    }
    static PostAsForm(url, parameters, onSuccess, onError) {
        let xhr = new XMLHttpRequest();
        xhr.open('POST', url, true);
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {
                onSuccess(xhr.responseText);
            }
        };
        xhr.onerror = (err) => {
            if (onError) {
                onError(err);
            }
        };
        let form = new FormData();
        for (let p in parameters) {
            if (Array.isArray(parameters[p])) {
                parameters[p].forEach((item) => {
                    form.append(p, item);
                });
                continue;
            }
            form.append(p, parameters[p]);
        }
        xhr.send(form);
    }
    static Get(url, onSuccess, onError) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url, true);
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {
                onSuccess(xhr.responseText);
            }
        };
        xhr.onerror = (err) => {
            if (onError) {
                onError(err);
            }
        };
        xhr.send();
    }
    static HasNestedChildren(obj) {
        for (let p in obj) {
            if (typeof obj[p] == "object") {
                return true;
            }
        }
        return false;
    }
}


/***/ }),

/***/ "./src/modules/Media.ts":
/*!******************************!*\
  !*** ./src/modules/Media.ts ***!
  \******************************/
/*! exports provided: Media, Spinner, FolderCard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Media", function() { return Media; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Spinner", function() { return Spinner; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FolderCard", function() { return FolderCard; });
/* harmony import */ var _HttpRequest__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./HttpRequest */ "./src/modules/HttpRequest.ts");
/* harmony import */ var _Modal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Modal */ "./src/modules/Modal.ts");
/* harmony import */ var _css_spinner_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./css/spinner.css */ "./src/modules/css/spinner.css");
/* harmony import */ var _css_spinner_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_css_spinner_css__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _css_folder_card_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./css/folder-card.css */ "./src/modules/css/folder-card.css");
/* harmony import */ var _css_folder_card_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_css_folder_card_css__WEBPACK_IMPORTED_MODULE_3__);




const API = {
    folders: "/api/media/folders",
    images: "/api/media/images?folderid="
};
const FOLDER_ICON = "/images/folder.ico";
class Media {
    get onOk() {
        return this._onOk;
    }
    set onOk(value) {
        this._onOk = value;
        this.modal.OnOk = () => {
            this._onOk(this.selectedCard);
        };
    }
    constructor() {
        this.container = document.createElement("div");
        this.spinner = new Spinner();
        this.foldersPerRow = 5;
        this.imagesPerRow = 5;
        this.init();
    }
    init() {
        this.container.classList.add("flex");
        this.container.classList.add("flex-wrap");
        //this.container.style.height = "100%";
        this.modal = new _Modal__WEBPACK_IMPORTED_MODULE_1__["Modal"]();
        this.modal.height = "70vh";
        this.modal.width = "70vw";
        this.modal.Body.appendChild(this.container);
        this.GetFolders();
    }
    GetFolders() {
        this.CleanView();
        this.CreateSpinner();
        _HttpRequest__WEBPACK_IMPORTED_MODULE_0__["HttpRequest"].Get(API.folders, (res) => {
            let folders = JSON.parse(res);
            folders.forEach((folder) => {
                let id = Number(folder["id"]);
                let card = new FolderCard(id, folder["name"]);
                this.cardsInView.push(card);
                card.width = (100 / this.foldersPerRow) + "%";
                this.container.appendChild(card.htmlElement);
                card.onClick = (ev) => {
                    this.GetImages(id);
                };
            });
            this.spinner.Destroy();
        });
    }
    GetImages(folderId) {
        this.CleanView();
        this.CreateSpinner();
        _HttpRequest__WEBPACK_IMPORTED_MODULE_0__["HttpRequest"].Get(API.images + folderId, (res) => {
            let images = JSON.parse(res);
            images.forEach(image => {
                let card = new FolderCard(image["id"], image["name"]);
                card.data = image["path"];
                this.cardsInView.push(card);
                card.icon.src = image["path"];
                card.width = (100 / this.imagesPerRow) + "%";
                this.container.appendChild(card.htmlElement);
                card.onClick = (ev) => {
                    this.cardsInView.forEach(c => {
                        c.selected = false;
                    });
                    card.selected = true;
                    this.selectedCard = card;
                };
            });
            this.spinner.Destroy();
        });
    }
    CleanView() {
        this.container.innerHTML = "";
        this.cardsInView = new Array();
    }
    CreateSpinner() {
        this.spinner = new Spinner();
        this.spinner.width = "100%";
        this.spinner.height = "100%";
        this.container.appendChild(this.spinner.container);
    }
}
class Spinner {
    get width() {
        return this.container.style.width;
    }
    set width(value) {
        this.container.style.width = value;
    }
    get height() {
        return this.container.style.height;
    }
    set height(value) {
        this.container.style.height = value;
    }
    get color() {
        return this.section1.style.borderColor;
    }
    set color(value) {
        this.section1.style.borderTopColor =
            this.section2.style.borderTopColor =
                this.section3.style.borderTopColor = value;
    }
    get zIndex() {
        return Number(this.container.style.zIndex);
    }
    set zIndex(value) {
        this.container.style.zIndex = value.toString();
    }
    constructor() {
        this.container = document.createElement("div");
        this.ring = document.createElement("div");
        this.section1 = document.createElement("div");
        this.section2 = document.createElement("div");
        this.section3 = document.createElement("div");
        this.init();
        this.width = "64px";
        this.height = "64px";
        this.zIndex = 100;
    }
    init() {
        this.container.classList.add("spinner-container");
        this.ring.classList.add("spinner-ring");
        this.container.appendChild(this.ring);
        this.ring.appendChild(this.section1);
        this.ring.appendChild(this.section2);
        this.ring.appendChild(this.section3);
    }
    Destroy() {
        this.container.parentNode.removeChild(this.container);
    }
}
class FolderCard {
    get id() {
        return Number(this.htmlElement.dataset.id);
    }
    set id(value) {
        this.htmlElement.dataset.id = value.toString();
    }
    get name() {
        return this.label.innerText;
    }
    set name(value) {
        this.label.innerText = value;
    }
    get onClick() {
        return this._onClick;
    }
    set onClick(value) {
        this._onClick = value;
        this.htmlElement.onclick = this._onClick;
    }
    get width() {
        return this.htmlElement.style.width;
    }
    set width(value) {
        this.htmlElement.style.width = value;
        this.htmlElement.style.maxWidth = value;
    }
    get selected() {
        return this._selected;
    }
    set selected(value) {
        this._selected = value;
        if (value) {
            this.htmlElement.classList.add("folder-card-selected");
        }
        else {
            this.htmlElement.classList.remove("folder-card-selected");
        }
    }
    constructor(id = 0, name = "New Folder") {
        this.htmlElement = document.createElement("div");
        this.icon = document.createElement("img");
        this.label = document.createElement("div");
        this.id = id;
        this.name = name;
        this.data = {};
        this.init();
    }
    init() {
        this.htmlElement.className = "folder-card";
        this.icon.src = FOLDER_ICON;
        this.icon.style.width = "100%";
        this.label.className = "folder-card-name";
        this.htmlElement.appendChild(this.icon);
        this.htmlElement.appendChild(this.label);
    }
}


/***/ }),

/***/ "./src/modules/Modal.ts":
/*!******************************!*\
  !*** ./src/modules/Modal.ts ***!
  \******************************/
/*! exports provided: Modal, ModalInput, ModalInputResult */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Modal", function() { return Modal; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalInput", function() { return ModalInput; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalInputResult", function() { return ModalInputResult; });
/* harmony import */ var _css_modal_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./css/modal.css */ "./src/modules/css/modal.css");
/* harmony import */ var _css_modal_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_css_modal_css__WEBPACK_IMPORTED_MODULE_0__);

class Modal {
    constructor() {
        this._OnOk = new Function();
        this._OnCancel = new Function();
        this._OnClose = new Function();
        this.Background = document.createElement('div');
        this.Container = document.createElement('div');
        this.Header = document.createElement('div');
        this.Body = document.createElement('div');
        this.Footer = document.createElement('div');
        this.CloseBtn = document.createElement('button');
        this.CancelBtn = document.createElement('button');
        this.OkBtn = document.createElement('button');
        this.Inputs = new Array();
        this.SetDefaultValues();
        this.Init();
        Object.seal(this);
    }
    get width() {
        return this.Container.style.width;
    }
    set width(value) {
        this.Container.style.width = value;
    }
    get height() {
        return this.Container.style.height;
    }
    set height(value) {
        this.Container.style.height = value;
    }
    get OnOk() {
        return this._OnOk;
    }
    set OnOk(value) {
        this.InitEvents();
        this._OnOk = value;
    }
    get OnClose() {
        return this._OnClose;
    }
    set OnClose(value) {
        this.InitEvents();
        this._OnClose = value;
    }
    get OnCancel() {
        return this._OnCancel;
    }
    set OnCancel(value) {
        this.InitEvents();
        this._OnCancel = value;
    }
    SetDefaultValues() {
        this.Background = (document.createElement('div'));
        this.Container = (document.createElement('div'));
        this.Header = (document.createElement('div'));
        this.Body = (document.createElement('div'));
        this.Footer = (document.createElement('div'));
        this.CloseBtn = (document.createElement('button'));
        this.CancelBtn = (document.createElement('button'));
        this.OkBtn = (document.createElement('button'));
        this.OnClose = new Function();
        this.OnCancel = new Function();
        this.OnOk = new Function();
        this.Inputs = new Array();
        this.Background.classList.add('modal-window-background');
        this.Container.classList.add('modal-window-container');
        this.Header.classList.add('modal-window-header');
        this.Body.classList.add('modal-window-body');
        this.Footer.classList.add('modal-window-footer');
        this.CloseBtn.classList.add('modal-window-close', 'btn', 'btn-danger');
        this.CloseBtn.innerText = 'x';
        this.CancelBtn.classList.add('modal-window-cancel', 'btn', 'btn-danger');
        this.CancelBtn.innerText = 'cancel';
        this.OkBtn.classList.add('modal-window-ok', 'btn', 'btn-success');
        this.OkBtn.innerText = 'ok';
    }
    Init() {
        document.body.appendChild(this.Background);
        this.Background.appendChild(this.Container);
        this.Container.appendChild(this.Header);
        this.Container.appendChild(this.Body);
        this.Container.appendChild(this.Footer);
        this.Header.appendChild(this.CloseBtn);
        this.Footer.appendChild(this.CancelBtn);
        this.Footer.appendChild(this.OkBtn);
        this.InitEvents();
    }
    InitInputs() {
        this.Inputs.forEach((item) => {
            this.Body.appendChild(item.Element);
        });
    }
    InitEvents() {
        this.CloseBtn.onclick = (e) => {
            this.OnClose(e);
            this.Destroy();
        };
        this.CancelBtn.onclick = (e) => {
            this.OnCancel(e);
            this.Destroy();
        };
        this.OkBtn.onclick = (e) => {
            let res = this.Inputs.map((item) => {
                return new ModalInputResult(item.Id, item.Name, item.Value);
            });
            this.OnOk(res, e);
            this.Destroy();
        };
    }
    Destroy() {
        this.Background.remove();
    }
    AddInput(input) {
        this.Inputs.push(input);
        this.RefreshInputs();
    }
    RemoveInput(id) {
        this.Inputs = this.Inputs.filter((item) => { item.Id !== id; });
        this.RefreshInputs();
    }
    RefreshInputs() {
        this.Body.innerHTML = "";
        this.InitInputs();
        this.InitEvents();
    }
}
class ModalInput {
    constructor() {
        this._Type = 'text';
        this._Name = "";
        this._Label = "";
        this.Element = document.createElement('input');
        this.LabelElement = document.createElement('label');
        this.Id = ModalInput.Identity;
        ModalInput.Identity++;
        this.Element = (document.createElement('input'));
        this.Element.type = this.Type;
    }
    get Value() {
        return this.Element.value;
    }
    set Value(value) {
        this.Element.value = value;
    }
    get Type() {
        return this._Type;
    }
    set Type(value) {
        this.Element.type = value;
        this._Type = value;
    }
    get Name() {
        return this._Name;
    }
    set Name(value) {
        this.Element.name = value;
        this._Name = value;
    }
    get Label() {
        return this._Label;
    }
    set Label(value) {
        if (this.LabelElement == null) {
            this.LabelElement = (document.createElement('label'));
            ;
        }
        this.LabelElement.innerText = value;
        this._Label = value;
    }
}
ModalInput.Identity = 1;
class ModalInputResult {
    constructor(id, name, value) {
        this.Id = id;
        this.Name = name;
        this.Value = value;
    }
}


/***/ }),

/***/ "./src/modules/ProductCard.ts":
/*!************************************!*\
  !*** ./src/modules/ProductCard.ts ***!
  \************************************/
/*! exports provided: ProductCard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductCard", function() { return ProductCard; });
/* harmony import */ var _css_ProductCard__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./css/ProductCard */ "./src/modules/css/ProductCard.css");
/* harmony import */ var _css_ProductCard__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_css_ProductCard__WEBPACK_IMPORTED_MODULE_0__);

class ProductCard {
    get OnClick() {
        return this.Container.onclick;
    }
    set OnClick(value) {
        this.Container.onclick = (event) => {
            value(event);
        };
    }
    get Price() {
        return this.PriceElement.innerText;
    }
    set Price(value) {
        this.PriceElement.innerText = value;
    }
    get MainImageUrl() {
        return this.ImageElement.src;
    }
    set MainImageUrl(value) {
        if (value) {
            this.ImageElement.src = value;
        }
    }
    get Name() {
        return this.Label.innerText;
    }
    set Name(value) {
        this.Label.innerText = value;
    }
    constructor() {
        this.Container = document.createElement("div");
        this.ImageContainer = document.createElement("div");
        this.Label = document.createElement("label");
        this.ImageElement = document.createElement("img");
        this.PriceElement = document.createElement("div");
        this.ImagesUrls = new Array();
        this.Price = "0";
        console.log(typeof this.Container);
        this.Init();
    }
    Init() {
        this.Container.className = "product-container";
        this.ImageContainer.className = "product-image-container";
        this.Label.className = "product-label";
        this.ImageElement.className = "product-image";
        this.PriceElement.className = "product-price";
        this.Container.appendChild(this.ImageContainer);
        this.ImageContainer.appendChild(this.ImageElement);
        this.Container.appendChild(this.Label);
        this.Container.appendChild(this.PriceElement);
    }
}


/***/ }),

/***/ "./src/modules/css/ProductCard.css":
/*!*****************************************!*\
  !*** ./src/modules/css/ProductCard.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader/dist/cjs.js!./ProductCard.css */ "./node_modules/css-loader/dist/cjs.js!./src/modules/css/ProductCard.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./src/modules/css/folder-card.css":
/*!*****************************************!*\
  !*** ./src/modules/css/folder-card.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader/dist/cjs.js!./folder-card.css */ "./node_modules/css-loader/dist/cjs.js!./src/modules/css/folder-card.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./src/modules/css/modal.css":
/*!***********************************!*\
  !*** ./src/modules/css/modal.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader/dist/cjs.js!./modal.css */ "./node_modules/css-loader/dist/cjs.js!./src/modules/css/modal.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./src/modules/css/spinner.css":
/*!*************************************!*\
  !*** ./src/modules/css/spinner.css ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader/dist/cjs.js!./spinner.css */ "./node_modules/css-loader/dist/cjs.js!./src/modules/css/spinner.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./src/shop/Index.ts":
/*!***************************!*\
  !*** ./src/shop/Index.ts ***!
  \***************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _modules_HttpRequest__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../modules/HttpRequest */ "./src/modules/HttpRequest.ts");
/* harmony import */ var _modules_ProductCard__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../modules/ProductCard */ "./src/modules/ProductCard.ts");
/* harmony import */ var _modules_Media__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../modules/Media */ "./src/modules/Media.ts");



let container = document.getElementById("products-container");
let spinner = new _modules_Media__WEBPACK_IMPORTED_MODULE_2__["Spinner"]();
spinner.width = container.clientWidth + "px";
spinner.height = spinner.width;
container.appendChild(spinner.container);
_modules_HttpRequest__WEBPACK_IMPORTED_MODULE_0__["HttpRequest"].Get("/api/products/get", (res) => {
    let products = JSON.parse(res);
    products.forEach((product) => {
        let images = product["images"];
        let id = product["id"];
        let card = new _modules_ProductCard__WEBPACK_IMPORTED_MODULE_1__["ProductCard"]();
        card.Name = product["name"];
        card.Description = product["description"];
        card.Price = product["price"];
        card.ImagesUrls = images.map((i) => i["path"]);
        card.MainImageUrl = card.ImagesUrls[0];
        container.appendChild(card.Container);
        card.OnClick = (e) => {
            window.location.assign(`/shop/details?=${id}`);
        };
    });
    spinner.Destroy();
});


/***/ })

/******/ });
//# sourceMappingURL=Index.js.map