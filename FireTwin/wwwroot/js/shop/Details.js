/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/shop/Details.ts");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/modules/HttpRequest.ts":
/*!************************************!*\
  !*** ./src/modules/HttpRequest.ts ***!
  \************************************/
/*! exports provided: HttpRequest */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpRequest", function() { return HttpRequest; });
class HttpRequest {
    static Post(url, parameters, onSuccess, onError) {
        let xhr = new XMLHttpRequest();
        xhr.open('POST', url, true);
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {
                onSuccess(xhr.responseText);
            }
        };
        xhr.onerror = (err) => {
            if (onError) {
                onError(err);
            }
        };
        if (HttpRequest.HasNestedChildren(parameters)) {
            xhr.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
            let data = JSON.stringify(parameters);
            xhr.send(data);
        }
        else {
            let form = new FormData();
            for (let p in parameters) {
                form.append(p, parameters[p]);
            }
            xhr.send(form);
        }
    }
    static PostAsForm(url, parameters, onSuccess, onError) {
        let xhr = new XMLHttpRequest();
        xhr.open('POST', url, true);
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {
                onSuccess(xhr.responseText);
            }
        };
        xhr.onerror = (err) => {
            if (onError) {
                onError(err);
            }
        };
        let form = new FormData();
        for (let p in parameters) {
            if (Array.isArray(parameters[p])) {
                parameters[p].forEach((item) => {
                    form.append(p, item);
                });
                continue;
            }
            form.append(p, parameters[p]);
        }
        xhr.send(form);
    }
    static Get(url, onSuccess, onError) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url, true);
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {
                onSuccess(xhr.responseText);
            }
        };
        xhr.onerror = (err) => {
            if (onError) {
                onError(err);
            }
        };
        xhr.send();
    }
    static HasNestedChildren(obj) {
        for (let p in obj) {
            if (typeof obj[p] == "object") {
                return true;
            }
        }
        return false;
    }
}


/***/ }),

/***/ "./src/shop/Details.ts":
/*!*****************************!*\
  !*** ./src/shop/Details.ts ***!
  \*****************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _modules_HttpRequest__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../modules/HttpRequest */ "./src/modules/HttpRequest.ts");

let AddToCartBtn = document.getElementById("add-to-cart");
let shoppingCartEl = document.getElementsByClassName("shopping-cart-count")[0];
let productId = document.getElementById("product-id").value;
AddToCartBtn.onclick = () => {
    _modules_HttpRequest__WEBPACK_IMPORTED_MODULE_0__["HttpRequest"].Post('/shoppingcart/addtocart', { productId: productId }, (res) => {
        console.log(res);
        shoppingCartEl.innerText = res;
    });
};


/***/ })

/******/ });
//# sourceMappingURL=Details.js.map