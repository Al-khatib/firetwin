﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DynEntity;
using FireTwin.Extras;
using FireTwin.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FireTwin.API
{
    [Authorize(Policy = "AdminOnly")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class MediaController : ControllerBase
    {
        private readonly DataRepository<Folder> _folders;
        public MediaController(DataRepository<Folder> folders)
        {
            _folders = folders;
        }
        public async Task<JsonResult> Folders()
        {
            IEnumerable<Folder> folders = _folders.All();
            JsonResult json = folders.Select(f => new { Id = f.Id, Name = f.Name }).ToJsonResult();
            return json;
        }

        public async Task<JsonResult> Images(int folderId)
        {
            Folder folder = _folders.Find(folderId);
            var json = folder?.Images?.Select(i => new { i.Id, i.Name, i.Path }).ToJsonResult();
            return json;
        }
    }
}