﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DynEntity;
using FireTwin.Extras;
using FireTwin.Models;
using Microsoft.AspNetCore.Mvc;

namespace FireTwin.API
{

    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ProductsController : Controller
    {
        private readonly DataRepository<Product> _products;
        public ProductsController(DataRepository<Product> products)
        {
            _products = products;
        }
        public async Task<JsonResult> Get()
        {
            IEnumerable<Product> model = _products.All();
            JsonResult json = model.Select(p => new
            {
                p.Id,
                p.Name,
                p.Price,
                p.LongDescription,
                p.Description,
                Images = p.Images.Select(i => new { i.Path })
            }).ToJsonResult();
            return json;
        }





    }
}