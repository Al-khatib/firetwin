﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FireTwin.Data.DynamicEntity
{
    public class EntityWraper
    {
        private readonly ApplicationDbContext _context;
        public EntityWraper(ApplicationDbContext context)
        {
            _context = context;
        }

        public DataRepository<T> GetInstance<T>() where T : DynamicEntity
        {
            return new DataRepository<T>(_context);
        }
    }
}
