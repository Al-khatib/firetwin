﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FireTwin.Data.DynamicEntity
{
    public class DynamicEntityRelation
    {
        public int Id { get; set; }
        public int Base { get; set; }
        public int Target { get; set; }

    }
}
