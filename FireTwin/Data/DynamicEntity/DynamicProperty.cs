﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FireTwin.Data.DynamicEntity
{
    public enum RelationType
    {
        None,
        SingleRelation,
        MultiRelation
    }
    public class DynamicProperty
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }

        public string ModelName { get; set; }
        public RelationType RelationType { get; set; }

        public virtual Guid ModelId { get; set; }
        public virtual List<DynamicProperty> Relations { get; set; } = new List<DynamicProperty>();


    }
}
