﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace FireTwin.Data.DynamicEntity
{
    public class DataRepository<T> where T : DynamicEntity
    {
        private readonly DbContext _context;
        private readonly DbSet<DynamicProperty> db;
        private readonly string ModelName = typeof(T).FullName;
        //DbContext is not added to services 
        //TODO: find a way to add dbcontext as scoped automatically if not scooped
        public DataRepository(ApplicationDbContext context)
        {
            _context = context;
            db = context.Set<DynamicProperty>();

        }

        public List<T> All()
        {

            var filtered = db.Where(p => p.ModelName == ModelName).ToList();
            var groups = filtered.GroupBy(p => p.ModelId);
            var groupedModelProperties = groups.Select(g => g.ToList()).ToList();
            var result = groupedModelProperties.Select(g => ToModel(g) as T).ToList();

            return result;
        }

        public int Add(T model)
        {

            int id = AddUnstrict(model);
            return id;
        }

        public List<int> AddRange(IEnumerable<T> models)
        {
            List<int> ids = new List<int>();
            List<DynamicProperty> properties = new List<DynamicProperty>();
            foreach (var model in models)
            {
                List<DynamicProperty> innerProperties = ToProperties(model);
                int id = innerProperties.First(p => p.Name == "Id").Id;
                ids.Add(id);
                properties.AddRange(innerProperties);
            }
            _context.AddRange(properties);
            _context.SaveChanges();
            return ids;
        }

        public T Find(Guid id)
        {
            List<DynamicProperty> properties = db.Where(p =>
            p.ModelId == id &&
            p.ModelName == ModelName)
            .ToList();

            T model = ToModel(properties) as T;
            return model;
        }

        public T Find(int id)
        {
            if (id < 1) return null;
            //We can get the modelId from any property that belongs to the model
            DynamicProperty property = db.FirstOrDefault(p => p.Id == id);
            if (property == null) return null;
            Guid modelId = property.ModelId;
            List<DynamicProperty> properties = db.Where(p =>
            p.ModelId == modelId &&
            p.ModelName == ModelName)
            .ToList();
            T model = ToModel(properties) as T;
            return model;

        }



        public void Remove(int id)
        {
            Guid modelId = GetModelId(id);
            IEnumerable<DynamicProperty> properties = db.Where(p => p.ModelId == modelId);
            db.RemoveRange(properties);
            _context.SaveChanges();
        }

        public int Update(T model)
        {
            int result = UpdateUnstrict(model);
            return result;
        }


        private DynamicEntity ToModel(IEnumerable<DynamicProperty> properties)
        {
            if (properties == null || !properties.Any()) return null;

            Dictionary<string, object> pairs = new Dictionary<string, object>();
            Type type = Type.GetType(properties.FirstOrDefault().ModelName);
            foreach (var property in properties)
            {
                object value = GetPropertyValue(property);
                pairs.Add(property.Name, value);
            }
            if (!pairs.Any()) return null;

            string serialized = JsonConvert.SerializeObject(pairs);
            DynamicEntity result = JsonConvert.DeserializeObject(serialized, type) as DynamicEntity;

            return result;
        }


        private List<DynamicProperty> ToProperties(DynamicEntity model)
        {
            string modelName = model.GetType().FullName;
            List<DynamicProperty> result = new List<DynamicProperty>();
            List<DynamicEntityRelation> relations = new List<DynamicEntityRelation>();
            PropertyInfo[] properties = model.GetType().GetProperties();

            Guid modelId = model.Id > 0 ? GetModelId(model.Id) : Guid.NewGuid();

            foreach (var p in properties)
            {
                object value = p.GetValue(model);
                RelationType relationType = GetRelationType(p.PropertyType);
                DynamicProperty property = new DynamicProperty();
                property.ModelId = modelId;
                property.ModelName = modelName;
                property.Name = p.Name;
                property.Type = p.PropertyType.FullName;
                property.Value = p.PropertyType.IsPrimitive || p.PropertyType == typeof(string) ?
                    p.GetValue(model)?.ToString() :
                    null;
                property.RelationType = relationType;
                result.Add(property);


                if (value != null)
                {
                    if (relationType == RelationType.SingleRelation)
                    {
                        List<DynamicProperty> nested = ToProperties(value as DynamicEntity);
                        property.Relations.AddRange(nested);
                        //result.AddRange(nested);
                    }
                    else if (relationType == RelationType.MultiRelation)
                    {
                        IEnumerable<DynamicEntity> asEnumable = value as IEnumerable<DynamicEntity>;
                        foreach (var item in asEnumable)
                        {
                            List<DynamicProperty> nested = ToProperties(item);
                            property.Relations.AddRange(nested);
                            //result.AddRange(nested);
                        }
                    }
                }
            }
            return result;
        }

        private int UpdateUnstrict(DynamicEntity model)
        {
            DynamicProperty idProperty = db.Find(model.Id);
            bool dbExist = idProperty != null ? true : false;
            if (dbExist)
            {
                Guid modelId = idProperty.ModelId;
                List<DynamicProperty> dbProperties = db.Where(p => p.ModelId == modelId && p.Name.ToLower() != "id").ToList();
                PropertyInfo[] properties = model.GetType().GetProperties();
                foreach (PropertyInfo info in properties)
                {
                    string name = info.Name;
                    if (name.ToLower() == "id") continue;
                    object value = info.GetValue(model);
                    DynamicProperty dbProperty = dbProperties.FirstOrDefault(p => p.Name == name);
                    string dbValue = dbProperty.Value;
                    RelationType relationType = GetRelationType(info.PropertyType);
                    if (relationType == RelationType.SingleRelation)
                    {
                        DynamicEntity casted = value as DynamicEntity;
                        UpdateUnstrict(casted);
                    }
                    else if (relationType == RelationType.MultiRelation)
                    {
                        IEnumerable<DynamicEntity> casted = value as IEnumerable<DynamicEntity>;
                        List<int> updated = new List<int>();
                        dbProperty.Relations = new List<DynamicProperty>();
                        foreach (var item in casted)
                        {
                            //IEnumerable<DynamicProperty> asProps = ToProperties(item);
                            //dbProperty.Relations.AddRange(asProps);
                            int id = UpdateUnstrict(item);
                            DynamicEntity entity = FindUnstrict(id);
                            dbProperty.Relations.AddRange(ToProperties(entity));
                        }

                        //bool isEqual = dbProperty.Relations.SequenceEqual(casted);
                        //if (isEqual) continue;
                        //dbProperty.Relations = casted.ToList();
                    }
                    else
                    {
                        if (value.ToString() == dbValue) continue;
                        dbProperty.Value = info.PropertyType.IsPrimitive || info.PropertyType == typeof(string) ?
                            value.ToString() :
                            null;
                    }
                }

            }
            else
            {
                model.Id = AddUnstrict(model);
            }

            _context.SaveChanges();
            return model.Id;
        }

        private DynamicEntity FindUnstrict(int id)
        {
            if (id < 1) return null;
            //We can get the modelId from any property that belongs to the model
            DynamicProperty property = db.FirstOrDefault(p => p.Id == id);
            if (property == null) return null;
            Guid modelId = property.ModelId;
            List<DynamicProperty> properties = db
                .Where(p => p.ModelId == modelId)
                .ToList();
            T model = ToModel(properties) as T;
            return model;
        }
        private int AddUnstrict(DynamicEntity model)
        {
            List<DynamicProperty> properties = ToProperties(model);
            _context.AddRange(properties);
            _context.SaveChanges();
            int id = properties.FirstOrDefault(p => p.Name == "Id").Id;
            return id;
        }
        private RelationType GetRelationType(Type type)
        {
            if (typeof(DynamicEntity).IsAssignableFrom(type))
            {
                return RelationType.SingleRelation;
            }
            else if (typeof(IEnumerable<DynamicEntity>).IsAssignableFrom(type))
            {
                return RelationType.MultiRelation;
            }
            else
            {
                return RelationType.None;
            }
        }

        private object GetPropertyValue(DynamicProperty property)
        {
            //object value = property.Name == "Id" ? (object)property.ModelId : property.Value;   
            object value = property.Name == "Id" ? (object)property.Id : property.Value;
            if (property.RelationType == RelationType.SingleRelation)
            {
                value = ToModel(property.Relations);
            }
            else if (property.RelationType == RelationType.MultiRelation)
            {
                List<DynamicEntity> models = new List<DynamicEntity>();
                var groups = property.Relations.GroupBy(g => g.ModelId);
                foreach (var group in groups)
                {
                    IEnumerable<DynamicProperty> modelProperties = group.Select(g => g);
                    DynamicEntity model = ToModel(modelProperties);
                    models.Add(model);
                }
                value = models.Any() ? models : null;
            }

            return value;
        }

        private Guid GetModelId(int id)
        {
            Guid modelId = db.FirstOrDefault(p => p.Id == id).ModelId;
            return modelId;
        }
    }
}
