﻿using DynEntity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace FireTwin.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {

        public DbSet<DynamicProperty> ModelProperties { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<DynamicRelation>()
                .HasKey(x => new { x.BaseId, x.TargetId });
            builder.Entity<DynamicRelation>()
                .HasOne(x => x.Base)
                .WithMany(x => x.RelationTarget)
                .HasForeignKey(x => x.BaseId)
                .OnDelete(DeleteBehavior.Restrict);
            builder.Entity<DynamicRelation>()
                .HasOne(x => x.Target)
                .WithMany(x => x.Relation)
                .HasForeignKey(x => x.TargetId)
                .OnDelete(DeleteBehavior.Restrict);

        }

    }
}
