﻿var glob = require("glob");


var baseUrl = "./src/";


var paths = glob.sync(baseUrl + "**/*.ts", {
    ignore: [baseUrl + "modules/**", baseUrl + "**/modules/**"]
});

var entries = {};

paths.forEach(function (item) {
    var key = item.replace(baseUrl, "").replace(".ts", "");
    entries[key] = item;
});

module.exports = {
    mode: "development",
    entry: entries,
    output: {
        path: __dirname + "/wwwroot/js",
        filename: "[name].js"

    },
    resolve: {
        extensions: [".ts", ".webpack.js", ".web.js", ".js", ".css", ".*"]
    },
    devtool: "source-map",
    module: {
        rules: [
            {
                test: /\.ts$/,
                loader: "awesome-typescript-loader"
            },
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"]
            }
        ]
    }
};