import { HttpRequest } from "../modules/HttpRequest";
let AddToCartBtn = document.getElementById("add-to-cart");
let shoppingCartEl = document.getElementsByClassName("shopping-cart-count")[0];
let productId = document.getElementById("product-id").value;
AddToCartBtn.onclick = () => {
    HttpRequest.Post('/shoppingcart/addtocart', { productId: productId }, (res) => {
        console.log(res);
        shoppingCartEl.innerText = res;
    });
};
