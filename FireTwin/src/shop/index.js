import { HttpRequest } from "../modules/HttpRequest";
import { ProductCard } from "../modules/ProductCard";
import { Spinner } from "../modules/Media";
let container = document.getElementById("products-container");
let spinner = new Spinner();
spinner.width = container.clientWidth + "px";
spinner.height = spinner.width;
container.appendChild(spinner.container);
HttpRequest.Get("/api/products/get", (res) => {
    let products = JSON.parse(res);
    products.forEach((product) => {
        let images = product["images"];
        let id = product["id"];
        let card = new ProductCard();
        card.Name = product["name"];
        card.Description = product["description"];
        card.Price = product["price"];
        card.ImagesUrls = images.map((i) => i["path"]);
        card.MainImageUrl = card.ImagesUrls[0];
        container.appendChild(card.Container);
        card.OnClick = (e) => {
            window.location.assign(`/shop/details?=${id}`);
        };
    });
    spinner.Destroy();
});
