﻿import "./css/modal.css";


export class Modal {
    private _OnOk: Function = new Function();
    private _OnCancel: Function = new Function();
    private _OnClose: Function = new Function();
    Background: HTMLDivElement = document.createElement('div');
    Container: HTMLDivElement = document.createElement('div');

    Header: HTMLDivElement = document.createElement('div');
    Body: HTMLDivElement = document.createElement('div');
    Footer: HTMLDivElement = document.createElement('div');
    CloseBtn: HTMLButtonElement = document.createElement('button');
    CancelBtn: HTMLButtonElement = document.createElement('button');
    OkBtn: HTMLButtonElement = document.createElement('button');
    Inputs: Array<ModalInput> = new Array<ModalInput>();

    get width() {
        return this.Container.style.width;
    }
    set width(value: string) {
        this.Container.style.width = value;
    }
    get height() {
        return this.Container.style.height;
    }
    set height(value: string) {
        this.Container.style.height = value;
    }

    get OnOk(): Function {
        return this._OnOk;
    }
    set OnOk(value: Function) {
        this.InitEvents();
        this._OnOk = value;
    }

    get OnClose(): Function {
        return this._OnClose;
    }
    set OnClose(value: Function) {
        this.InitEvents();
        this._OnClose = value;
    }

    get OnCancel(): Function {
        return this._OnCancel;
    }
    set OnCancel(value: Function) {
        this.InitEvents();
        this._OnCancel = value;
    }

    constructor() {

        this.SetDefaultValues();
        this.Init();
        Object.seal<Modal>(this);
    }

    SetDefaultValues() {
        this.Background = <HTMLDivElement>(document.createElement('div'));
        this.Container = <HTMLDivElement>(document.createElement('div'));
        this.Header = <HTMLDivElement>(document.createElement('div'));
        this.Body = <HTMLDivElement>(document.createElement('div'));
        this.Footer = <HTMLDivElement>(document.createElement('div'));
        this.CloseBtn = <HTMLButtonElement>(document.createElement('button'));
        this.CancelBtn = <HTMLButtonElement>(document.createElement('button'));
        this.OkBtn = <HTMLButtonElement>(document.createElement('button'));
        this.OnClose = new Function();
        this.OnCancel = new Function();
        this.OnOk = new Function();
        this.Inputs = new Array<ModalInput>();


        this.Background.classList.add('modal-window-background');
        this.Container.classList.add('modal-window-container');
        this.Header.classList.add('modal-window-header');
        this.Body.classList.add('modal-window-body');
        this.Footer.classList.add('modal-window-footer');
        this.CloseBtn.classList.add('modal-window-close', 'btn', 'btn-danger');
        this.CloseBtn.innerText = 'x';
        this.CancelBtn.classList.add('modal-window-cancel', 'btn', 'btn-danger');
        this.CancelBtn.innerText = 'cancel';
        this.OkBtn.classList.add('modal-window-ok', 'btn', 'btn-success');
        this.OkBtn.innerText = 'ok';

    }

    Init() {

        document.body.appendChild(this.Background);
        this.Background.appendChild(this.Container);
        this.Container.appendChild(this.Header);
        this.Container.appendChild(this.Body);
        this.Container.appendChild(this.Footer);
        this.Header.appendChild(this.CloseBtn);
        this.Footer.appendChild(this.CancelBtn);
        this.Footer.appendChild(this.OkBtn);

        this.InitEvents();
    }

    InitInputs() {

        this.Inputs.forEach((item) => {
            this.Body.appendChild(item.Element);
        });

    }


    InitEvents() {
        this.CloseBtn.onclick = (e) => {
            this.OnClose(e);
            this.Destroy();
        }
        this.CancelBtn.onclick = (e) => {
            this.OnCancel(e);
            this.Destroy();
        }
        this.OkBtn.onclick = (e) => {
            let res = this.Inputs.map((item) => {
                return new ModalInputResult(item.Id, item.Name, item.Value)
            });
            this.OnOk(res, e);
            this.Destroy();
        }
    }
    Destroy() {
        this.Background.remove();
    }

    AddInput(input: ModalInput) {
        this.Inputs.push(input);
        this.RefreshInputs();
    }

    RemoveInput(id: number) {
        this.Inputs = this.Inputs.filter((item) => { item.Id !== id });
        this.RefreshInputs();
    }

    RefreshInputs() {
        this.Body.innerHTML = "";
        this.InitInputs();
        this.InitEvents();
    }

}

export class ModalInput {
    static Identity: number = 1;
    private _Type: string = 'text';
    private _Name: string = "";
    private _Label: string = "";
    Element: HTMLInputElement = document.createElement('input');
    LabelElement: HTMLLabelElement = document.createElement('label');
    readonly Id: number;

    get Value(): any {
        return this.Element.value;
    }
    set Value(value: any) {
        this.Element.value = value;
    }
    get Type(): string {
        return this._Type;
    }
    set Type(value: string) {
        this.Element.type = value;
        this._Type = value;
    }

    get Name(): string {
        return this._Name;
    }
    set Name(value: string) {
        this.Element.name = value
        this._Name = value;
    }


    get Label(): string {
        return this._Label;
    }
    set Label(value: string) {
        if (this.LabelElement == null) {
            this.LabelElement = <HTMLLabelElement>(document.createElement('label'));;
        }
        this.LabelElement.innerText = value;
        this._Label = value;
    }


    constructor() {
        this.Id = ModalInput.Identity;
        ModalInput.Identity++;
        this.Element = <HTMLInputElement>(document.createElement('input'));
        this.Element.type = this.Type;

    }


}

export class ModalInputResult {
    Id: number;
    Name: string;
    Value: any;
    constructor(id: number, name: string, value: any) {
        this.Id = id;
        this.Name = name;
        this.Value = value;
    }
}



