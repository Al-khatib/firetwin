﻿
import "./css/ProductCard";


export class ProductCard {
    Container: HTMLDivElement;
    ImageContainer: HTMLDivElement;
    Label: HTMLLabelElement;
    ImageElement: HTMLImageElement;
    PriceElement: HTMLDivElement;

    ImagesUrls: Array<string>;
    Description: string;

    get OnClick() {
        return this.Container.onclick;
    }
    set OnClick(value: (e: MouseEvent) => void) {
        this.Container.onclick = (event: MouseEvent) => {
            value(event);
        }
    }

    get Price(): string {
        return this.PriceElement.innerText;
    }
    set Price(value: string) {
        this.PriceElement.innerText = value;
    }
    get MainImageUrl(): string {
        return this.ImageElement.src;
    }
    set MainImageUrl(value: string) {
        if (value) {
            this.ImageElement.src = value;
        }
    }
    get Name(): string {
        return this.Label.innerText;
    }

    set Name(value: string) {
        this.Label.innerText = value;
    }

    constructor() {
        this.Container = document.createElement("div");
        this.ImageContainer = document.createElement("div");
        this.Label = document.createElement("label");
        this.ImageElement = document.createElement("img");
        this.PriceElement = document.createElement("div");
        this.ImagesUrls = new Array<string>();
        this.Price = "0";

        console.log(typeof this.Container);
        this.Init();
    }

    Init() {
        this.Container.className = "product-container";
        this.ImageContainer.className = "product-image-container";
        this.Label.className = "product-label";
        this.ImageElement.className = "product-image";
        this.PriceElement.className = "product-price";


        this.Container.appendChild(this.ImageContainer);
        this.ImageContainer.appendChild(this.ImageElement);
        this.Container.appendChild(this.Label);
        this.Container.appendChild(this.PriceElement);
    }
}