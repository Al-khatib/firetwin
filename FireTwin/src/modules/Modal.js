import "./css/modal.css";
export class Modal {
    constructor() {
        this._OnOk = new Function();
        this._OnCancel = new Function();
        this._OnClose = new Function();
        this.Background = document.createElement('div');
        this.Container = document.createElement('div');
        this.Header = document.createElement('div');
        this.Body = document.createElement('div');
        this.Footer = document.createElement('div');
        this.CloseBtn = document.createElement('button');
        this.CancelBtn = document.createElement('button');
        this.OkBtn = document.createElement('button');
        this.Inputs = new Array();
        this.SetDefaultValues();
        this.Init();
        Object.seal(this);
    }
    get width() {
        return this.Container.style.width;
    }
    set width(value) {
        this.Container.style.width = value;
    }
    get height() {
        return this.Container.style.height;
    }
    set height(value) {
        this.Container.style.height = value;
    }
    get OnOk() {
        return this._OnOk;
    }
    set OnOk(value) {
        this.InitEvents();
        this._OnOk = value;
    }
    get OnClose() {
        return this._OnClose;
    }
    set OnClose(value) {
        this.InitEvents();
        this._OnClose = value;
    }
    get OnCancel() {
        return this._OnCancel;
    }
    set OnCancel(value) {
        this.InitEvents();
        this._OnCancel = value;
    }
    SetDefaultValues() {
        this.Background = (document.createElement('div'));
        this.Container = (document.createElement('div'));
        this.Header = (document.createElement('div'));
        this.Body = (document.createElement('div'));
        this.Footer = (document.createElement('div'));
        this.CloseBtn = (document.createElement('button'));
        this.CancelBtn = (document.createElement('button'));
        this.OkBtn = (document.createElement('button'));
        this.OnClose = new Function();
        this.OnCancel = new Function();
        this.OnOk = new Function();
        this.Inputs = new Array();
        this.Background.classList.add('modal-window-background');
        this.Container.classList.add('modal-window-container');
        this.Header.classList.add('modal-window-header');
        this.Body.classList.add('modal-window-body');
        this.Footer.classList.add('modal-window-footer');
        this.CloseBtn.classList.add('modal-window-close', 'btn', 'btn-danger');
        this.CloseBtn.innerText = 'x';
        this.CancelBtn.classList.add('modal-window-cancel', 'btn', 'btn-danger');
        this.CancelBtn.innerText = 'cancel';
        this.OkBtn.classList.add('modal-window-ok', 'btn', 'btn-success');
        this.OkBtn.innerText = 'ok';
    }
    Init() {
        document.body.appendChild(this.Background);
        this.Background.appendChild(this.Container);
        this.Container.appendChild(this.Header);
        this.Container.appendChild(this.Body);
        this.Container.appendChild(this.Footer);
        this.Header.appendChild(this.CloseBtn);
        this.Footer.appendChild(this.CancelBtn);
        this.Footer.appendChild(this.OkBtn);
        this.InitEvents();
    }
    InitInputs() {
        this.Inputs.forEach((item) => {
            this.Body.appendChild(item.Element);
        });
    }
    InitEvents() {
        this.CloseBtn.onclick = (e) => {
            this.OnClose(e);
            this.Destroy();
        };
        this.CancelBtn.onclick = (e) => {
            this.OnCancel(e);
            this.Destroy();
        };
        this.OkBtn.onclick = (e) => {
            let res = this.Inputs.map((item) => {
                return new ModalInputResult(item.Id, item.Name, item.Value);
            });
            this.OnOk(res, e);
            this.Destroy();
        };
    }
    Destroy() {
        this.Background.remove();
    }
    AddInput(input) {
        this.Inputs.push(input);
        this.RefreshInputs();
    }
    RemoveInput(id) {
        this.Inputs = this.Inputs.filter((item) => { item.Id !== id; });
        this.RefreshInputs();
    }
    RefreshInputs() {
        this.Body.innerHTML = "";
        this.InitInputs();
        this.InitEvents();
    }
}
export class ModalInput {
    constructor() {
        this._Type = 'text';
        this._Name = "";
        this._Label = "";
        this.Element = document.createElement('input');
        this.LabelElement = document.createElement('label');
        this.Id = ModalInput.Identity;
        ModalInput.Identity++;
        this.Element = (document.createElement('input'));
        this.Element.type = this.Type;
    }
    get Value() {
        return this.Element.value;
    }
    set Value(value) {
        this.Element.value = value;
    }
    get Type() {
        return this._Type;
    }
    set Type(value) {
        this.Element.type = value;
        this._Type = value;
    }
    get Name() {
        return this._Name;
    }
    set Name(value) {
        this.Element.name = value;
        this._Name = value;
    }
    get Label() {
        return this._Label;
    }
    set Label(value) {
        if (this.LabelElement == null) {
            this.LabelElement = (document.createElement('label'));
            ;
        }
        this.LabelElement.innerText = value;
        this._Label = value;
    }
}
ModalInput.Identity = 1;
export class ModalInputResult {
    constructor(id, name, value) {
        this.Id = id;
        this.Name = name;
        this.Value = value;
    }
}
