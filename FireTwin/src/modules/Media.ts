﻿import { HttpRequest } from "./HttpRequest";
import { Modal } from "./Modal";
import "./css/spinner.css";
import "./css/folder-card.css";

const API = {
    folders: "/api/media/folders",
    images: "/api/media/images?folderid="
}

const FOLDER_ICON = "/images/folder.ico";

export class Media {
    private _onOk: (selected: FolderCard) => void;

    container: HTMLDivElement;
    spinner: Spinner;
    modal: Modal;
    foldersPerRow: number;
    imagesPerRow: number;
    selectedCard: FolderCard;
    cardsInView: Array<FolderCard>;

    get onOk(): (selected: FolderCard) => void {
        return this._onOk;
    }
    set onOk(value: (selected: FolderCard) => void) {
        this._onOk = value;
        this.modal.OnOk = () => {
            this._onOk(this.selectedCard);
        }
    }

    constructor() {
        this.container = <HTMLDivElement>document.createElement("div");
        this.spinner = new Spinner();
        this.foldersPerRow = 5;
        this.imagesPerRow = 5;
        this.init();
    }

    init() {
        this.container.classList.add("flex");
        this.container.classList.add("flex-wrap");
        //this.container.style.height = "100%";

        this.modal = new Modal();
        this.modal.height = "70vh";
        this.modal.width = "70vw";
        this.modal.Body.appendChild(this.container);
        this.GetFolders();
    }
    GetFolders() {
        this.CleanView();
        this.CreateSpinner();
        HttpRequest.Get(API.folders, (res) => {
            let folders = <Array<object>>JSON.parse(res);
            folders.forEach((folder) => {
                let id = Number(folder["id"]);
                let card = new FolderCard(id, folder["name"]);
                this.cardsInView.push(card);
                card.width = (100 / this.foldersPerRow) + "%";
                this.container.appendChild(card.htmlElement);
                card.onClick = (ev: MouseEvent) => {
                    this.GetImages(id);
                }
            });
            this.spinner.Destroy();
        });
    }
    GetImages(folderId: number) {
        this.CleanView();
        this.CreateSpinner();
        HttpRequest.Get(API.images + folderId, (res) => {
            let images = <Array<object>>JSON.parse(res);
            images.forEach(image => {
                let card = new FolderCard(image["id"], image["name"]);
                card.data = image["path"];
                this.cardsInView.push(card);
                card.icon.src = image["path"];
                card.width = (100 / this.imagesPerRow) + "%";
                this.container.appendChild(card.htmlElement);
                card.onClick = (ev) => {
                    this.cardsInView.forEach(c => {
                        c.selected = false;
                    });
                    card.selected = true;
                    this.selectedCard = card;
                }
            });

            this.spinner.Destroy();
        });
    }
    CleanView() {
        this.container.innerHTML = "";
        this.cardsInView = new Array<FolderCard>();
    }
    CreateSpinner() {
        this.spinner = new Spinner();
        this.spinner.width = "100%";
        this.spinner.height = "100%";
        this.container.appendChild(this.spinner.container);
    }

}

export class Spinner {
    get width() {
        return this.container.style.width;
    }
    set width(value: string) {
        this.container.style.width = value;
    }
    get height() {
        return this.container.style.height;
    }
    set height(value: string) {
        this.container.style.height = value;
    }
    get color() {
        return this.section1.style.borderColor;
    }
    set color(value: string) {
        this.section1.style.borderTopColor =
            this.section2.style.borderTopColor =
            this.section3.style.borderTopColor = value;
    }

    get zIndex() {
        return Number(this.container.style.zIndex);
    }

    set zIndex(value: number) {
        this.container.style.zIndex = value.toString();
    }

    ring: HTMLDivElement;
    section1: HTMLDivElement;
    section2: HTMLDivElement;
    section3: HTMLDivElement;
    container: HTMLDivElement;
    constructor() {
        this.container = <HTMLDivElement>document.createElement("div");
        this.ring = <HTMLDivElement>document.createElement("div");
        this.section1 = <HTMLDivElement>document.createElement("div");
        this.section2 = <HTMLDivElement>document.createElement("div");
        this.section3 = <HTMLDivElement>document.createElement("div");
        this.init();
        this.width = "64px";
        this.height = "64px";
        this.zIndex = 100;
    }

    init() {
        this.container.classList.add("spinner-container");
        this.ring.classList.add("spinner-ring");
        this.container.appendChild(this.ring);
        this.ring.appendChild(this.section1);
        this.ring.appendChild(this.section2);
        this.ring.appendChild(this.section3);

    }
    Destroy() {
        this.container.parentNode.removeChild(this.container);
    }
}

export class FolderCard {
    private _onClick: (ev: MouseEvent) => any;
    private _selected: boolean;
    data: {};
    get id(): number {
        return Number(this.htmlElement.dataset.id);
    }
    set id(value: number) {
        this.htmlElement.dataset.id = value.toString();
    }
    get name(): string {
        return this.label.innerText;
    }
    set name(value: string) {
        this.label.innerText = value;
    }
    get onClick(): (ev: MouseEvent) => any {
        return this._onClick;
    }
    set onClick(value: (ev: MouseEvent) => any) {
        this._onClick = value;
        this.htmlElement.onclick = this._onClick;
    }
    get width(): string {
        return this.htmlElement.style.width;
    }
    set width(value: string) {
        this.htmlElement.style.width = value;
        this.htmlElement.style.maxWidth = value;
    }
    get selected() {
        return this._selected;
    }
    set selected(value: boolean) {
        this._selected = value;
        if (value) {
            this.htmlElement.classList.add("folder-card-selected");
        } else {
            this.htmlElement.classList.remove("folder-card-selected");
        }
    }
    htmlElement: HTMLDivElement;
    icon: HTMLImageElement;
    label: HTMLDivElement;
    constructor(id: number = 0, name: string = "New Folder") {
        this.htmlElement = document.createElement("div");
        this.icon = document.createElement("img");
        this.label = document.createElement("div");
        this.id = id;
        this.name = name;
        this.data = {};
        this.init();

    }

    init() {
        this.htmlElement.className = "folder-card";
        this.icon.src = FOLDER_ICON;
        this.icon.style.width = "100%";
        this.label.className = "folder-card-name";

        this.htmlElement.appendChild(this.icon);
        this.htmlElement.appendChild(this.label);
    }
}


