import "./css/ProductCard";
export class ProductCard {
    get OnClick() {
        return this.Container.onclick;
    }
    set OnClick(value) {
        this.Container.onclick = (event) => {
            value(event);
        };
    }
    get Price() {
        return this.PriceElement.innerText;
    }
    set Price(value) {
        this.PriceElement.innerText = value;
    }
    get MainImageUrl() {
        return this.ImageElement.src;
    }
    set MainImageUrl(value) {
        if (value) {
            this.ImageElement.src = value;
        }
    }
    get Name() {
        return this.Label.innerText;
    }
    set Name(value) {
        this.Label.innerText = value;
    }
    constructor() {
        this.Container = document.createElement("div");
        this.ImageContainer = document.createElement("div");
        this.Label = document.createElement("label");
        this.ImageElement = document.createElement("img");
        this.PriceElement = document.createElement("div");
        this.ImagesUrls = new Array();
        this.Price = "0";
        console.log(typeof this.Container);
        this.Init();
    }
    Init() {
        this.Container.className = "product-container";
        this.ImageContainer.className = "product-image-container";
        this.Label.className = "product-label";
        this.ImageElement.className = "product-image";
        this.PriceElement.className = "product-price";
        this.Container.appendChild(this.ImageContainer);
        this.ImageContainer.appendChild(this.ImageElement);
        this.Container.appendChild(this.Label);
        this.Container.appendChild(this.PriceElement);
    }
}
