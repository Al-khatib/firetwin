﻿
export class HttpRequest {
    static Post(url: string, parameters: object, onSuccess: (result: any) => void, onError?: (err: any) => void) {
        let xhr = new XMLHttpRequest();
        xhr.open('POST', url, true);
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {
                onSuccess(xhr.responseText);
            }
        }
        xhr.onerror = (err) => {
            if (onError) {
                onError(err);
            }
        }

        if (HttpRequest.HasNestedChildren(parameters)) {
            xhr.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
            let data: string = JSON.stringify(parameters);
            xhr.send(data);
        }
        else {
            let form = new FormData();
            for (let p in parameters) {
                form.append(p, parameters[p]);
            }
            xhr.send(form);
        }
    }

    static PostAsForm(url: string, parameters: any, onSuccess: (result: any) => void, onError?: (err: any) => void) {
        let xhr = new XMLHttpRequest();
        xhr.open('POST', url, true);
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {
                onSuccess(xhr.responseText);
            }
        }
        xhr.onerror = (err) => {
            if (onError) {
                onError(err);
            }
        }

        let form = new FormData();
        for (let p in parameters) {
            if (Array.isArray(parameters[p])) {
                parameters[p].forEach((item) => {
                    form.append(p, item);
                });
                continue;
            }
            form.append(p, parameters[p]);
        }
        xhr.send(form);

    }

    static Get(url: string, onSuccess: (result: any) => void, onError?: (err: any) => void) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url, true);

        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {
                onSuccess(xhr.responseText);
            }
        }
        xhr.onerror = (err) => {
            if (onError) {
                onError(err);
            }
        }

        xhr.send();
    }

    static HasNestedChildren(obj: object) {
        for (let p in obj) {
            if (typeof obj[p] == "object") {
                return true;
            }
        }
        return false;
    }
}

