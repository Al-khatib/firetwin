import { HttpRequest } from "./HttpRequest";
import { Modal } from "./Modal";
import "./css/spinner.css";
import "./css/folder-card.css";
const API = {
    folders: "/api/media/folders",
    images: "/api/media/images?folderid="
};
const FOLDER_ICON = "/images/folder.ico";
export class Media {
    get onOk() {
        return this._onOk;
    }
    set onOk(value) {
        this._onOk = value;
        this.modal.OnOk = () => {
            this._onOk(this.selectedCard);
        };
    }
    constructor() {
        this.container = document.createElement("div");
        this.spinner = new Spinner();
        this.foldersPerRow = 5;
        this.imagesPerRow = 5;
        this.init();
    }
    init() {
        this.container.classList.add("flex");
        this.container.classList.add("flex-wrap");
        //this.container.style.height = "100%";
        this.modal = new Modal();
        this.modal.height = "70vh";
        this.modal.width = "70vw";
        this.modal.Body.appendChild(this.container);
        this.GetFolders();
    }
    GetFolders() {
        this.CleanView();
        this.CreateSpinner();
        HttpRequest.Get(API.folders, (res) => {
            let folders = JSON.parse(res);
            folders.forEach((folder) => {
                let id = Number(folder["id"]);
                let card = new FolderCard(id, folder["name"]);
                this.cardsInView.push(card);
                card.width = (100 / this.foldersPerRow) + "%";
                this.container.appendChild(card.htmlElement);
                card.onClick = (ev) => {
                    this.GetImages(id);
                };
            });
            this.spinner.Destroy();
        });
    }
    GetImages(folderId) {
        this.CleanView();
        this.CreateSpinner();
        HttpRequest.Get(API.images + folderId, (res) => {
            let images = JSON.parse(res);
            images.forEach(image => {
                let card = new FolderCard(image["id"], image["name"]);
                card.data = image["path"];
                this.cardsInView.push(card);
                card.icon.src = image["path"];
                card.width = (100 / this.imagesPerRow) + "%";
                this.container.appendChild(card.htmlElement);
                card.onClick = (ev) => {
                    this.cardsInView.forEach(c => {
                        c.selected = false;
                    });
                    card.selected = true;
                    this.selectedCard = card;
                };
            });
            this.spinner.Destroy();
        });
    }
    CleanView() {
        this.container.innerHTML = "";
        this.cardsInView = new Array();
    }
    CreateSpinner() {
        this.spinner = new Spinner();
        this.spinner.width = "100%";
        this.spinner.height = "100%";
        this.container.appendChild(this.spinner.container);
    }
}
export class Spinner {
    get width() {
        return this.container.style.width;
    }
    set width(value) {
        this.container.style.width = value;
    }
    get height() {
        return this.container.style.height;
    }
    set height(value) {
        this.container.style.height = value;
    }
    get color() {
        return this.section1.style.borderColor;
    }
    set color(value) {
        this.section1.style.borderTopColor =
            this.section2.style.borderTopColor =
                this.section3.style.borderTopColor = value;
    }
    get zIndex() {
        return Number(this.container.style.zIndex);
    }
    set zIndex(value) {
        this.container.style.zIndex = value.toString();
    }
    constructor() {
        this.container = document.createElement("div");
        this.ring = document.createElement("div");
        this.section1 = document.createElement("div");
        this.section2 = document.createElement("div");
        this.section3 = document.createElement("div");
        this.init();
        this.width = "64px";
        this.height = "64px";
        this.zIndex = 100;
    }
    init() {
        this.container.classList.add("spinner-container");
        this.ring.classList.add("spinner-ring");
        this.container.appendChild(this.ring);
        this.ring.appendChild(this.section1);
        this.ring.appendChild(this.section2);
        this.ring.appendChild(this.section3);
    }
    Destroy() {
        this.container.parentNode.removeChild(this.container);
    }
}
export class FolderCard {
    get id() {
        return Number(this.htmlElement.dataset.id);
    }
    set id(value) {
        this.htmlElement.dataset.id = value.toString();
    }
    get name() {
        return this.label.innerText;
    }
    set name(value) {
        this.label.innerText = value;
    }
    get onClick() {
        return this._onClick;
    }
    set onClick(value) {
        this._onClick = value;
        this.htmlElement.onclick = this._onClick;
    }
    get width() {
        return this.htmlElement.style.width;
    }
    set width(value) {
        this.htmlElement.style.width = value;
        this.htmlElement.style.maxWidth = value;
    }
    get selected() {
        return this._selected;
    }
    set selected(value) {
        this._selected = value;
        if (value) {
            this.htmlElement.classList.add("folder-card-selected");
        }
        else {
            this.htmlElement.classList.remove("folder-card-selected");
        }
    }
    constructor(id = 0, name = "New Folder") {
        this.htmlElement = document.createElement("div");
        this.icon = document.createElement("img");
        this.label = document.createElement("div");
        this.id = id;
        this.name = name;
        this.data = {};
        this.init();
    }
    init() {
        this.htmlElement.className = "folder-card";
        this.icon.src = FOLDER_ICON;
        this.icon.style.width = "100%";
        this.label.className = "folder-card-name";
        this.htmlElement.appendChild(this.icon);
        this.htmlElement.appendChild(this.label);
    }
}
