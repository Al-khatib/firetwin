﻿import { Modal } from "../modules/Modal";
import { HttpRequest } from "../modules/HttpRequest";
import { Media, FolderCard } from "../modules/Media";

let addImageBtn = document.getElementById("add-image");
let imagesContainer = <HTMLDivElement>document.getElementById("images-container");

addImageBtn.onclick = (e) => {
    let media = new Media();
    media.onOk = (selected: FolderCard) => {
        let input = <HTMLInputElement>document.createElement("input");
        input.type = "hidden";
        input.name = "Images";
        input.value = selected.id.toString();
        let img = <HTMLImageElement>document.createElement("img");
        img.height = 100;
        img.src = selected.data.toString();
        imagesContainer.appendChild(img);
        imagesContainer.appendChild(input);
    }
}
