import { Media } from "../modules/Media";
let addImageBtn = document.getElementById("add-image");
let imagesContainer = document.getElementById("images-container");
addImageBtn.onclick = (e) => {
    let media = new Media();
    media.onOk = (selected) => {
        let input = document.createElement("input");
        input.type = "hidden";
        input.name = "Images";
        input.value = selected.id.toString();
        let img = document.createElement("img");
        img.height = 100;
        img.src = selected.data.toString();
        imagesContainer.appendChild(img);
        imagesContainer.appendChild(input);
    };
};
