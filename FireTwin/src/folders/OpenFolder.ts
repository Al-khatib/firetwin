﻿import { HttpRequest as Req, HttpRequest } from "../modules/HttpRequest";


let addImageBtn = <HTMLInputElement>document.getElementById('add-image-input');
let imagesContainer = <HTMLDivElement>document.getElementById('images-container');

addImageBtn.onchange = (event) => {
    let input = <HTMLInputElement>event.target;

    let isValidTypes = true;
    for (var j = 0; j < input.files.length; j++) {
        if (input.files[j].type.split("/")[0] != "image") {
            isValidTypes = false;
            return;
        }
    }


    if (input.files.length > 0 && isValidTypes) {
        let reader = new FileReader();
        LoadFile(0, reader, input);

    }

}

document.body.addEventListener("click", (event) => {
    
    if (event.target instanceof Element) {
        let element: HTMLButtonElement = <HTMLButtonElement>event.target;
    
        if (element.classList.contains("image-delete-btn")) {
            let id: number = Number(element.dataset.imageId);
            DeleteImage(id, element);
        }
    }
})


function LoadFile(x: number, reader: any, input: HTMLInputElement) {

    reader.onload = () => {

        if (x == input.files.length - 1) {

            let files = [];
            for (var i = 0; i < input.files.length; i++) {
                files.push(input.files[i]);
            }
            Req.PostAsForm("/Folders/AddImage", { files: files, folderId: input.dataset.folderId }, (res) => {
                if (res && res.length > 0) {
                    JSON.parse(res).forEach((i, index) => {
                        let imgCard = <HTMLDivElement>document.createElement('div');
                        imgCard.classList.add('image-card');
                        let imgItem = <HTMLDivElement>document.createElement('div');
                        imgItem.classList.add('image-item');
                        let imgLabel = <HTMLDivElement>document.createElement('div');
                        imgLabel.classList.add('image-label');
                        let imgSpan = <HTMLSpanElement>document.createElement('span');
                        imgSpan.innerText = files[index].name;
                        let img = <HTMLImageElement>document.createElement('img');
                        img.src = i;
                        imagesContainer.append(imgCard);
                        imgCard.appendChild(imgItem);
                        imgItem.appendChild(img);
                        imgCard.appendChild(imgLabel);
                        imgLabel.appendChild(imgSpan);
                    });
                }

            });
            return;
        }
        LoadFile(x + 1, reader, input);
    }
    reader.readAsText(input.files[x]);
}

function DeleteImage(id: number, button: HTMLButtonElement ) {
    let folderId = addImageBtn.dataset.folderId;
    Req.Post("/Folders/DeleteImage", { folderId: folderId, imageId: id }, (result) => {
        if (result) {
            let imageCard = button.parentElement.parentElement;
            imageCard.parentNode.removeChild(imageCard);
        }
    });
    
    
    

}