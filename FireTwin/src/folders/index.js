import { Modal, ModalInput } from "../modules/Modal";
import { HttpRequest as Req } from "../modules/HttpRequest";
let btn = document.getElementById('btn');
let url = btn.dataset.url;
btn.onclick = () => {
    let modal = new Modal();
    let input = new ModalInput();
    modal.AddInput(input);
    modal.OnOk = (res) => {
        let data = { folderName: res[0].Value };
        Req.Post(url, data, (result) => {
            if (result != "0") {
                let folderElement = CreateFolderElement(result, data.folderName);
                document.getElementById('folders-container').appendChild(folderElement);
            }
        });
    };
};
function CreateFolderElement(id, name) {
    let div = document.createElement('div');
    let anchor = document.createElement('a');
    let img = document.createElement('img');
    let h2 = document.createElement('h2');
    div.classList.add("icon-folder");
    anchor.href = "/Folders/OpenFolder/" + id;
    img.src = "/images/folder.ico";
    img.alt = "Folder";
    h2.classList.add("folder-name");
    h2.innerText = name;
    anchor.appendChild(img);
    div.appendChild(anchor);
    div.appendChild(h2);
    return div;
}
