﻿using System.Threading.Tasks;
using DynEntity;
using FireTwin.Models;
using Microsoft.AspNetCore.Mvc;

namespace FireTwin.Controllers
{
    public class ShopController : Controller
    {
        private readonly DataRepository<Product> _prod;
        public ShopController(DataRepository<Product> prod)
        {

            //_products = db.GetInstance<Product>();
            _prod = prod;
        }
        public async Task<IActionResult> Index()
        {
            var hh = _prod.All();
            return View();
        }

        public async Task<IActionResult> Details(int id)
        {
            Product model = _prod.Find(id);
            return View(model);
        }
    }
}