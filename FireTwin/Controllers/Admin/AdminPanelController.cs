﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FireTwin.Extras.CustomDataNotations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FireTwin.Controllers.Admin
{
    [Authorize(Policy = "AdminOnly")]
    [ViewLayout(AdminLayout = true)]
    public class AdminPanelController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}