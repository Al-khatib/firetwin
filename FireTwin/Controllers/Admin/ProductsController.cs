﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FireTwin.Models;
using Microsoft.AspNetCore.Authorization;
using FireTwin.Models.ViewModels;
using System.Collections.Generic;
using System.Linq;
using FireTwin.Extras.CustomDataNotations;
using DynEntity;

namespace FireTwin.Controllers.Admin
{
    [Authorize(Policy = "AdminOnly")]
    [ViewLayout(AdminLayout = true)]
    public class ProductsController : Controller
    {
        private readonly DataRepository<Product> _products;
        private readonly DataRepository<Category> _categories;
        private readonly DataRepository<Image> _images;

        public ProductsController(
            DataRepository<Product> products,
            DataRepository<Category> categories,
            DataRepository<Image> images
            )
        {
            this._products = products;
            this._categories = categories;
            this._images = images;
        }

        //GET: Products
        public async Task<IActionResult> Index()
        {
            var model = _products.All();
            return View(model);
        }

        //// GET: Products/Details/5
        //public async Task<IActionResult> Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var product = await db.GetProduct(id ?? 0);
        //    if (product == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(product);
        //}

        // GET: Products/Create
        public IActionResult Create()
        {
            ProductCategoriesViewModel model = new ProductCategoriesViewModel();
            model.Categories = _categories.All();
            return View(model);
        }

        // POST: Products/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public async Task<IActionResult> Create([Bind("Id,Name,Description,LongDescription,ImageUrl")] Product product)
        public async Task<IActionResult> Create(ProductCategoriesViewModel productViewModel, int[] images)
        {
            if (ModelState.IsValid)
            {
                Category category =  _categories.Find(productViewModel.SelectedCategoryId);
                productViewModel.Product.Category = category;
                IEnumerable<Image> dbImages = images.ToList().Select(i => _images.Find(i)).ToList();
                productViewModel.Product.Images = dbImages;
                _products.Add(productViewModel.Product);
                return RedirectToAction(nameof(Index));
            }
            return View(productViewModel);
        }

        //// GET: Products/Edit/5
        //public async Task<IActionResult> Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var product = await db.GetProduct(id ?? 0);
        //    if (product == null)
        //    {
        //        return NotFound();
        //    }
        //    return View(product);
        //}

        //// POST: Products/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Description,LongDescription,ImageUrl")] Product product)
        //{
        //    if (id != product.Id)
        //    {
        //        return NotFound();
        //    }

        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            await db.Update(product);
        //        }
        //        catch (DbUpdateConcurrencyException)
        //        {
        //            if (!ProductExists(product.Id))
        //            {
        //                return NotFound();
        //            }
        //            else
        //            {
        //                throw;
        //            }
        //        }
        //        return RedirectToAction(nameof(Index));
        //    }
        //    return View(product);
        //}

        //// GET: Products/Delete/5
        //public async Task<IActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var product = await db.GetProduct(id ?? 0);
        //    if (product == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(product);
        //}

        //// POST: Products/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> DeleteConfirmed(int id)
        //{
        //    await db.Remove<Product>(id);
        //    return RedirectToAction(nameof(Index));
        //}

        //private bool ProductExists(int id)
        //{
        //    return db.GetProducts().Result.Any(e => e.Id == id);
        //}
    }
}
