﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DynEntity;
using FireTwin.Extras;
using FireTwin.Extras.CustomDataNotations;
using FireTwin.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FireTwin.Controllers
{
    [Authorize(Policy = "AdminOnly")]
    [ViewLayout(AdminLayout = true)]
    public class FoldersController : Controller
    {
        private readonly DataRepository<Folder> _folders;
        private readonly DataRepository<Image> _images;


        public FoldersController(DataRepository<Folder> folders, DataRepository<Image> images)
        {
            _folders = folders;
            _images = images;
        }

        //GET: Folders
        public async Task<IActionResult> Index()
        {
            var model = _folders.All();
            return View(model);
        }

        [HttpPost]
        public async Task<int> AddFolder(string folderName)
        {

            if (!string.IsNullOrWhiteSpace(folderName))
            {
                Folder folder = new Folder() { Name = folderName, Images = new List<Image>() };
                int id = _folders.Add(folder);
                string folderPath = string.Format("{0}\\{1}", Bundler.HostingEnviroment.WebRootPath, Bundler.MediaFolder);
                Directory.CreateDirectory(folderPath + "\\" + folderName);
                return id;
            }
            else
            {
                return 0;
            }
        }


        public async Task<IActionResult> OpenFolder(int Id)
        {
            Folder folder = _folders.Find(Id);
            if (folder.Images == null)
            {
                folder.Images = new List<Image>();
            }
            return View(folder);
        }

        [HttpPost]
        public async Task<bool> DeleteImage(int folderId, int imageId)
        {
            Folder folder = _folders.Find(folderId);
            Image image = folder.Images.FirstOrDefault(i => i.Id == imageId);
            if (image != null)
            {
                folder.Images.Remove(image);
                _folders.Update(folder);
                _images.Remove(imageId);
                //db.SaveChanges();
                return true;
            }

            return false;
        }

        [HttpPost]
        public async Task<IEnumerable<string>> AddImage(List<IFormFile> files, int folderId)
        {
            if (files == null) return null;
            bool unvalid = files.Any(f => !f.ContentType.StartsWith("image/"));
            if (unvalid) return null;

            Folder folder = _folders.Find(folderId);
            IEnumerable<Image> updatedImages = await CreateImage(folder, files);
            return updatedImages.Select(i => i.Path);
        }


        private async Task<IEnumerable<Image>> CreateImage(Folder folder, List<IFormFile> files)
        {
            List<Image> result = new List<Image>();
            string folderpath = string.Format("{0}\\{1}\\{2}", Bundler.HostingEnviroment.WebRootPath, Bundler.MediaFolder, folder.Name);
            //Will create directory if doesnt exists
            Directory.CreateDirectory(folderpath);

            foreach (var file in files)
            {
                Image image = new Image { Name = file.FileName };
                image.Path = string.Format("/{0}/{1}/{2}", Bundler.MediaFolder, folder.Name, file.FileName);
                if (folder.Images == null)
                {
                    folder.Images = new List<Image>();
                }
                folder.Images.Add(image);
                using (FileStream stream = new FileStream(folderpath + "\\" + file.FileName, FileMode.Create))
                {
                    file.CopyTo(stream);
                }

                result.Add(image);
            }
            _folders.Update(folder);

            return result;
        }


    }

}