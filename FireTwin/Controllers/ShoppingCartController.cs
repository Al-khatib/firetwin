﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using DynEntity;
using FireTwin.Extras;
using FireTwin.Models;
using Microsoft.AspNetCore.Mvc;

namespace FireTwin.Controllers
{
    public class ShoppingCartController : Controller
    {
        private readonly DataRepository<Cart> _carts;
        private readonly DataRepository<Product> _products;
        private const string SessionCartKey = "Cart";
        public ShoppingCartController(DataRepository<Cart> carts, DataRepository<Product> products)
        {
            _carts = carts;
            //_carts = db.GetInstance<Cart>();
            _products = products;
        }

        public async Task<IActionResult> Index()
        {
            Cart cart = await GetCartAsync();
            var products = cart.Products;
            return View(products);
        }

        [HttpPost]
        public async Task<int> AddToCart(int productId)
        {
            Cart cart = await GetCartAsync();
            Product product = _products.Find(productId);
            if (product != null)
            {
                cart.Products.Add(product);
                await SaveCartAsync(cart);
            }
            return cart.Products.Count(); ;
        }

        public async Task<int> CartCount()
        {
            Cart cart = await GetCartAsync();
            return cart.Products.Count();
        }

        private async Task<Cart> GetCartAsync()
        {
            Cart cart = null;
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                string userId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
                IEnumerable<Cart> carts = _carts.All();
                cart = carts.FirstOrDefault(c => c.UserId == userId);
                if (cart == null)
                {
                    cart = new Cart() { UserId = userId, Products = new List<Product>() };
                    cart.Id =  _carts.Add(cart);
                }
            }
            else
            {
                cart = HttpContext.Session.GetObjectFromJson<Cart>(SessionCartKey);
                if (cart == null)
                {
                    cart = new Cart() { Products = new List<Product>() };
                    HttpContext.Session.SetObjectAsJson(SessionCartKey, cart);
                }
            }


            return cart;
        }

        private async Task<bool> SaveCartAsync(Cart cart)
        {
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                _carts.Update(cart);
            }
            else
            {
                HttpContext.Session.SetObjectAsJson(SessionCartKey, cart);
            }
            return true;
        }

    }
}